const { getToken } = require("./helpers/common");

const express = require("express");
const axios = require("axios");
const cors = require("cors");
// const request = require("request-promise");
require("dotenv").config();

const app = express();
const port = 3001;

// CORS configuration for your React app
// app.use(cors());
app.use(
  cors({
    origin: "*", // Allow requests from any origin
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true, // Allow credentials if needed
  })
);
app.use(express.json());

app.get("/api/order/listings", async (req, res) => {
  try {
    // Replace with the actual URL of the API you want to call
    let token = await getToken();
    console.log("🚀  file: api.js:31  app.get ~ token:-----", token);

    const apiResponse = await axios.get(
      "https://apisandbox.uship.com/v2/listings",
      {
        headers: {
          Authorization: `Bearer ${token}`,
          "Access-Control-Allow-Origin":
            "https://enabling-kings-figures-fascinating.trycloudflare.com",
        },
      }
    );

    // await getShippingRate(token);

    let result = apiResponse?.data?.items;
    let totalData = apiResponse?.data?.totalCount;
    console.log("🚀 ~ file: index.js:43 ~ app.get ~ totalData:", totalData);

    if (totalData > 0) {
      result = result.map((r) => {
        return {
          order_id: `ID: ${r.listingId}`,
          title: r.title || "",
          pick_up_address:
            r?.route?.items?.length && r?.route?.items[0].address
              ? r?.route?.items[0]?.address?.label
              : "",

          pick_up_date:
            r?.route?.items?.length && r?.route?.items[0].timeFrame
              ? r?.route?.items[0]?.timeFrame?.label
              : "",
          delivery_address:
            r?.route?.items?.length && r?.route?.items[1].address
              ? r?.route?.items[1]?.address?.label
              : "",

          delivery_date:
            r?.route?.items?.length && r?.route?.items[1].timeFrame
              ? r?.route?.items[1]?.timeFrame?.label
              : "",

          customer_name:
            r?.route?.items?.length && r?.route?.items[1].contact
              ? r?.route?.items[1]?.contact?.name
              : "",
          mobile_no:
            r?.route?.items?.length && r?.route?.items[1].contact
              ? r?.route?.items[1]?.contact?.phoneNumber
              : "",

          // goods : {
          //   total :
          //   items : []
          // },
          price: r?.offerPrice?.label || "",
          status: r?.status?.label || "",
        };
      });
    }

    res.json({
      totalCount: totalData,
      items: result,
    });
    // res.json(apiResponse?.data);
  } catch (error) {
    console.log("🚀  file: api.js:25  app.get ~ error:", error);
    // Handle error
    res.status(500).json({ message: "Error fetching products", error: error });
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
