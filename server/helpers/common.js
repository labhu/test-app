const request = require("request-promise");
const axios = require("axios");

module.exports = {
  getToken: async () => {
    const options = {
      method: "POST",
      uri: "https://api.ushipsandbox.com/partner/oauth/token_authenticated",
      form: {
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET,
        grant_type: process.env.GRANT_TYPE,
        username: process.env.USER_NAME,
        password: process.env.PASSWORD,
      },
      json: true, // Automatically parses the JSON string in the response
    };

    try {
      const response = await request(options);
      const accessToken = response.access_token;

      // Use the access token as needed
      console.log("Access Token:", accessToken);

      // Respond with the access token
      return accessToken;
    } catch (error) {
      console.log("🚀 ~ file: common.js:34 ~ getToken: ~ error:", error);
    }
  },

  getShippingRate: async (token) => {
    try {
      const apiResponse = await axios.get(
        "https://api.uship.com/v2/fixedprice",
        {
          headers: {
            Authorization: `Bearer ${token}`,
            // "Access-Control-Allow-Origin":
            //   "https://enabling-kings-figures-fascinating.trycloudflare.com",
          },
        }
      );
      console.log(
        "🚀 ~ file: common.js:48 ~ getShippingRate: ~ apiResponse:",
        apiResponse
      );

      // Respond with the access token
      return apiResponse.data;
    } catch (error) {
      console.log("🚀 ~ file: common.js:56 ~ getShippingRate: ~ error:", error);
      throw error;
    }
  },
};
