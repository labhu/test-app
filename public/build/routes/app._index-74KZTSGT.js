import {
  require_shopify
} from "/build/_shared/chunk-SU66BP3D.js";
import {
  require_node
} from "/build/_shared/chunk-G7CHZRZX.js";
import {
  init_esm,
  useActionData
} from "/build/_shared/chunk-XNST2UG7.js";
import {
  Button,
  Card,
  Form,
  FormLayout,
  Layout,
  Page,
  Text,
  TextField,
  VerticalStack,
  init_esm as init_esm2
} from "/build/_shared/chunk-RYZ2NSIR.js";
import "/build/_shared/chunk-HEETWZQQ.js";
import "/build/_shared/chunk-GIAAE3CH.js";
import {
  require_jsx_dev_runtime
} from "/build/_shared/chunk-XU7DNSPJ.js";
import {
  require_react
} from "/build/_shared/chunk-BOXFZXVX.js";
import {
  createHotContext,
  init_remix_hmr
} from "/build/_shared/chunk-BYNBTF7W.js";
import "/build/_shared/chunk-UWV35TSL.js";
import {
  __toESM
} from "/build/_shared/chunk-PNG5AS42.js";

// app/routes/app._index.jsx
init_remix_hmr();
var import_react = __toESM(require_react());

// app/routes/assets/styles.css
var styles_default = "/build/_assets/styles-2SRR7RMW.css";

// app/routes/app._index.jsx
var import_node = __toESM(require_node());
init_esm();
init_esm2();

// app/routes/assets/logo.jsx
init_remix_hmr();
var import_jsx_dev_runtime = __toESM(require_jsx_dev_runtime());
if (!window.$RefreshReg$ || !window.$RefreshSig$ || !window.$RefreshRuntime$) {
  console.warn("remix:hmr: React Fast Refresh only works when the Remix compiler is running in development mode.");
} else {
  prevRefreshReg = window.$RefreshReg$;
  prevRefreshSig = window.$RefreshSig$;
  window.$RefreshReg$ = (type, id) => {
    window.$RefreshRuntime$.register(type, '"app/routes/assets/logo.jsx"' + id);
  };
  window.$RefreshSig$ = window.$RefreshRuntime$.createSignatureFunctionForTransform;
}
var prevRefreshReg;
var prevRefreshSig;
if (import.meta) {
  import.meta.hot = createHotContext(
    //@ts-expect-error
    "app/routes/assets/logo.jsx"
  );
  import.meta.hot.lastModified = "1699424614461.258";
}
var Logo = () => {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("svg", { width: "71", height: "36", viewBox: "0 0 71 36", fill: "none", xmlns: "http://www.w3.org/2000/svg", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M41.3926 19.5992H36.1616V15.0412H31.4906V30.2032H36.1616V24.2672H41.3926V30.2032H46.0636V15.0152H41.3926V19.5992Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 23,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M47.554 30.222H52.222V15.032H47.554V30.222Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 24,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M49.8995 8.8836C48.4095 8.8676 47.1885 10.0626 47.1725 11.5526C47.1725 11.5716 47.1725 11.5906 47.1725 11.6096C47.1715 13.1156 48.3925 14.3376 49.8985 14.3376C51.4045 14.3386 52.6255 13.1186 52.6265 11.6116C52.6275 10.1056 51.4065 8.8846 49.9005 8.8836H49.8995Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 25,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M61.3934 25.8396C59.7504 25.8186 58.4234 24.4916 58.4014 22.8486C58.4014 21.2566 59.8004 19.8606 61.3934 19.8606C62.9864 19.8606 64.3814 21.2606 64.3814 22.8486C64.3814 24.4366 62.9854 25.8396 61.3934 25.8396ZM61.3934 15.4536C60.3954 15.4486 59.4064 15.6536 58.4934 16.0566L58.4014 16.0976V15.0376H53.6294V33.4306H58.4014V29.6246L58.4934 29.6656C59.4064 30.0676 60.3954 30.2726 61.3934 30.2666C65.5394 30.2666 68.9004 27.0056 68.9004 22.8596C68.9004 18.7136 65.5394 15.3526 61.3934 15.4536Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 26,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M11.0857 22.4418C11.0857 24.1568 9.6167 25.6068 7.8797 25.6068C6.1417 25.6068 4.6707 24.1638 4.6707 22.4418V15.0558H-0.000301361V22.4418C-0.000301361 26.7278 3.5347 30.2168 7.8747 30.2168C12.2147 30.2168 15.7487 26.7298 15.7487 22.4418V15.0558H11.0857V22.4418Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 27,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M69.0315 16.9757H68.7405V16.6367H69.0645C69.1235 16.6317 69.1825 16.6487 69.2305 16.6827C69.2655 16.7127 69.2845 16.7567 69.2835 16.8027C69.2835 16.8347 69.2735 16.8667 69.2555 16.8947C69.2375 16.9227 69.2095 16.9437 69.1775 16.9547C69.1305 16.9697 69.0805 16.9767 69.0315 16.9757ZM69.2385 17.1597L69.1715 17.0907L69.2595 17.0527C69.2945 17.0387 69.3255 17.0177 69.3515 16.9897C69.4015 16.9387 69.4275 16.8697 69.4255 16.7987C69.4255 16.7427 69.4105 16.6887 69.3815 16.6407C69.3555 16.5967 69.3145 16.5617 69.2665 16.5427C69.1985 16.5207 69.1265 16.5117 69.0555 16.5147H68.5955V17.5517H68.7365V17.0927H68.8925C68.9175 17.0897 68.9435 17.0897 68.9685 17.0927C68.9905 17.0977 69.0115 17.1077 69.0305 17.1197C69.0565 17.1397 69.0795 17.1627 69.0985 17.1897C69.1235 17.2207 69.1545 17.2657 69.1965 17.3317L69.3345 17.5477H69.5115L69.3295 17.2627C69.3025 17.2257 69.2725 17.1907 69.2385 17.1597Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 28,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M69.0223 17.8787C68.5583 17.8797 68.1813 17.5037 68.1813 17.0397C68.1803 16.5767 68.5563 16.1997 69.0193 16.1987C69.4833 16.1977 69.8603 16.5737 69.8613 17.0377V17.0397C69.8593 17.5027 69.4843 17.8767 69.0223 17.8777V17.8787ZM69.0223 16.0607C68.4823 16.0607 68.0443 16.4987 68.0443 17.0387C68.0443 17.5787 68.4823 18.0167 69.0223 18.0167C69.5623 18.0167 70.0003 17.5787 70.0003 17.0387C69.9993 16.4987 69.5623 16.0617 69.0223 16.0607Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 29,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M33.29 7.1342L37.34 8.5992L33.31 0.000198364L24.712 4.0332L28.414 5.3712L28.39 5.4342C28.1 6.1712 27.849 6.7252 27.75 6.8442C27.605 7.0162 27.407 7.2512 26.278 7.3262C26.005 7.3442 25.777 7.3522 25.557 7.3522H25.059C24.507 7.3582 23.958 7.4082 23.414 7.5012C21.812 7.7302 20.604 8.2682 19.485 9.2272C18.556 10.0302 17.631 11.0652 17.148 13.2142C16.806 14.7372 17.033 16.2472 17.822 17.7032C18.314 18.6152 19.439 20.0282 21.998 21.5862C23.148 22.2842 23.938 22.8272 24.316 23.3772C24.569 23.7472 24.675 24.1492 24.609 24.4812C24.578 24.6652 24.475 24.8282 24.321 24.9342C24.026 25.1252 23.451 25.2842 22.843 25.4512L22.803 25.4612C21.87 25.7242 20.713 26.0362 19.784 26.6432C18.367 27.5612 17.947 28.2772 16.712 33.7522L16.638 34.0762L21.279 35.3992L21.367 35.0442C21.805 33.2782 22.154 32.2402 22.397 31.9582C22.674 31.6382 22.833 31.5062 23.534 31.2882L23.968 31.1572L24.148 31.1032C25.185 30.8192 26.192 30.4382 27.159 29.9672C28.917 29.1102 30.161 26.9942 30.405 24.4442C30.668 21.7402 29.688 20.2682 28.596 19.0822C27.844 18.2682 24.564 16.1552 24.095 15.8762C23.518 15.5332 22.842 14.9572 22.895 14.1222C22.938 13.4662 23.644 13.1782 23.83 13.1342C23.972 13.1032 24.215 13.0792 24.598 13.0422H24.636C25.169 12.9922 25.973 12.9112 27.118 12.7472C29.311 12.4232 30.491 11.3632 30.896 10.9222C31.906 9.8332 32.711 8.5722 33.274 7.1982L33.29 7.1342Z", fill: "#02AAF3" }, void 0, false, {
      fileName: "app/routes/assets/logo.jsx",
      lineNumber: 30,
      columnNumber: 7
    }, this)
  ] }, void 0, true, {
    fileName: "app/routes/assets/logo.jsx",
    lineNumber: 22,
    columnNumber: 10
  }, this);
};
_c = Logo;
var _c;
$RefreshReg$(_c, "Logo");
window.$RefreshReg$ = prevRefreshReg;
window.$RefreshSig$ = prevRefreshSig;

// app/routes/app._index.jsx
var import_shopify = __toESM(require_shopify());
var import_jsx_dev_runtime2 = __toESM(require_jsx_dev_runtime());
if (!window.$RefreshReg$ || !window.$RefreshSig$ || !window.$RefreshRuntime$) {
  console.warn("remix:hmr: React Fast Refresh only works when the Remix compiler is running in development mode.");
} else {
  prevRefreshReg = window.$RefreshReg$;
  prevRefreshSig = window.$RefreshSig$;
  window.$RefreshReg$ = (type, id) => {
    window.$RefreshRuntime$.register(type, '"app/routes/app._index.jsx"' + id);
  };
  window.$RefreshSig$ = window.$RefreshRuntime$.createSignatureFunctionForTransform;
}
var prevRefreshReg;
var prevRefreshSig;
var _s = $RefreshSig$();
if (import.meta) {
  import.meta.hot = createHotContext(
    //@ts-expect-error
    "app/routes/app._index.jsx"
  );
}
var links = () => [{
  rel: "stylesheet",
  href: styles_default
}];
function Index() {
  var _a;
  _s();
  const [formState, setformState] = (0, import_react.useState)({
    email: "",
    password: "",
    unique_key: "",
    access_key: ""
  });
  const actionData = useActionData();
  const productId = (_a = actionData == null ? void 0 : actionData.product) == null ? void 0 : _a.id.replace("gid://shopify/Product/", "");
  const handleSubmit = (0, import_react.useCallback)(() => {
  }, []);
  const Openpopup = (0, import_react.useCallback)(() => {
    let get = document.querySelector("#popup");
    if (get) {
      get.classList.toggle("hide");
    }
  }, []);
  (0, import_react.useEffect)(() => {
    if (productId) {
      shopify.toast.show("Product created");
    }
  }, [productId]);
  const generateProduct = async () => {
    console.log(formState.email);
    console.log(formState.password);
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");
    await fetch("https://api-web.ushipdev.com/oauth/token_authenticated", {
      credentials: "include",
      // headers: {
      //   "Access-Control-Allow-Origin": "*", // Replace with your allowed origin(s)
      //   "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
      //   "Access-Control-Allow-Headers":
      //     "Origin, X-Requested-With, Content-Type, Accept",
      //   // 'x-frame-options' : 'none'
      // },
      method: "POST",
      body: JSON.stringify({
        client_id: "djc82cpkrkcbqr3xjtdgkb4u",
        client_secret: "G5tPdQEvty",
        grant_type: "password",
        username: formState.email,
        password: formState.password
      })
    }).then((response) => console.log(response, "RESSS")).catch((err) => console.error(err));
    setformState({
      email: "",
      password: "",
      access_key: "",
      unique_key: ""
    });
  };
  const handleChange = (field, value) => {
    setformState({
      ...formState,
      [field]: value
    });
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_jsx_dev_runtime2.Fragment, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(VerticalStack, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "login-page-container", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Layout, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "login-page-layout-container", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Card, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "login-page-layout-logo", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Logo, {}, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 200,
        columnNumber: 21
      }, this) }, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 199,
        columnNumber: 19
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "login-page-layout-text", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Text, { as: "h2", variant: "headingMd", children: "Access Uship Listing" }, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 204,
        columnNumber: 21
      }, this) }, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 203,
        columnNumber: 19
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Form, { onSubmit: handleSubmit, children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(FormLayout, { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(TextField, { label: "Access Key", type: "text", autoComplete: "none", placeholder: "Enter your access key here", name: "username", value: formState.access_key, onChange: (value) => handleChange("access_key", value), requiredIndicator: true }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 211,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Submit-Button", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Button, { primary: true, fullWidth: true, onClick: generateProduct, children: "Submit" }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 215,
          columnNumber: 25
        }, this) }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 214,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Forget-Button", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Button, { plain: true, fullWidth: true, onClick: Openpopup, children: "How to get access key?" }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 220,
          columnNumber: 25
        }, this) }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 219,
          columnNumber: 23
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 210,
        columnNumber: 21
      }, this) }, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 209,
        columnNumber: 19
      }, this)
    ] }, void 0, true, {
      fileName: "app/routes/app._index.jsx",
      lineNumber: 198,
      columnNumber: 17
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Access-key-popup hide", id: "popup", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Card, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Text, { as: "h2", variant: "headingMd", children: "Where to find Access key?" }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 236,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("ul", { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: [
            "Login to",
            " ",
            /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("a", { href: "https://login.ushipsandbox.com", className: "Access-key-popup-A", target: "_blank", rel: "noreferrer", children: "https://login.ushipsandbox.com/" }, void 0, false, {
              fileName: "app/routes/app._index.jsx",
              lineNumber: 242,
              columnNumber: 27
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 240,
            columnNumber: 25
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: "Go to (Menu-Name)(Sub Menu-Name)" }, void 0, false, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 247,
            columnNumber: 25
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("li", { children: "Click on API KEY GENERATION" }, void 0, false, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 248,
            columnNumber: 25
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 239,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("p", { children: "After following above steps you will be able to see below screen and can generate your unique key from here. This key can be used to access your uship account in shopify." }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 250,
          columnNumber: 23
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 235,
        columnNumber: 21
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Access-key-popup-form", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Api-form", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Text, { as: "h2", variant: "headingMd", children: "API KEY GENERATION" }, void 0, false, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 260,
          columnNumber: 27
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("p", { children: [
          "Generate a unique key below. Once generated, you can copuy/paste your key into your application. Note that generating a new key will invalidate a current existingkey . ",
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("b", { children: "Learn More" }, void 0, false, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 267,
            columnNumber: 43
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 263,
          columnNumber: 27
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("form", { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Generate-key-Button", children: /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Button, { onClick: handleSubmit, children: "Generate Key" }, void 0, false, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 271,
            columnNumber: 31
          }, this) }, void 0, false, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 270,
            columnNumber: 29
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("div", { className: "Api-form-input-box", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(TextField, { label: "YOUR UNIQUE KEY", type: "text", autoComplete: "none", placeholder: "Enter your unique key here", name: "username", value: formState.unique_key, onChange: (value) => handleChange("unique_key", value), requiredIndicator: true }, void 0, false, {
              fileName: "app/routes/app._index.jsx",
              lineNumber: 276,
              columnNumber: 31
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(Button, { onClick: handleSubmit, children: "\u2714" }, void 0, false, {
              fileName: "app/routes/app._index.jsx",
              lineNumber: 278,
              columnNumber: 31
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 275,
            columnNumber: 29
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 269,
          columnNumber: 27
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 259,
        columnNumber: 25
      }, this) }, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 258,
        columnNumber: 23
      }, this) }, void 0, false, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 257,
        columnNumber: 21
      }, this)
    ] }, void 0, true, {
      fileName: "app/routes/app._index.jsx",
      lineNumber: 234,
      columnNumber: 19
    }, this) }, void 0, false, {
      fileName: "app/routes/app._index.jsx",
      lineNumber: 233,
      columnNumber: 17
    }, this)
  ] }, void 0, true, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 197,
    columnNumber: 15
  }, this) }, void 0, false, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 196,
    columnNumber: 13
  }, this) }, void 0, false, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 195,
    columnNumber: 11
  }, this) }, void 0, false, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 194,
    columnNumber: 9
  }, this) }, void 0, false, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 193,
    columnNumber: 7
  }, this) }, void 0, false, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 192,
    columnNumber: 10
  }, this);
}
_s(Index, "2RM7HsYQBAcQyplSuvo34/xNqeM=", false, function() {
  return [useActionData];
});
_c2 = Index;
var _c2;
$RefreshReg$(_c2, "Index");
window.$RefreshReg$ = prevRefreshReg;
window.$RefreshSig$ = prevRefreshSig;
export {
  Index as default,
  links
};
//# sourceMappingURL=/build/routes/app._index-74KZTSGT.js.map
