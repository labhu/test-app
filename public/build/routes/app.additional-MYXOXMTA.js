import {
  Page,
  init_esm
} from "/build/_shared/chunk-RYZ2NSIR.js";
import "/build/_shared/chunk-HEETWZQQ.js";
import "/build/_shared/chunk-GIAAE3CH.js";
import {
  require_jsx_dev_runtime
} from "/build/_shared/chunk-XU7DNSPJ.js";
import {
  require_react
} from "/build/_shared/chunk-BOXFZXVX.js";
import {
  createHotContext,
  init_remix_hmr
} from "/build/_shared/chunk-BYNBTF7W.js";
import "/build/_shared/chunk-UWV35TSL.js";
import {
  __toESM
} from "/build/_shared/chunk-PNG5AS42.js";

// app/routes/app.additional.jsx
init_remix_hmr();
init_esm();

// app/routes/assets/app.additional.css
var app_additional_default = "/build/_assets/app.additional-ZKV6WDBF.css";

// app/routes/app.additional.jsx
var import_react = __toESM(require_react());
var import_jsx_dev_runtime = __toESM(require_jsx_dev_runtime());
if (!window.$RefreshReg$ || !window.$RefreshSig$ || !window.$RefreshRuntime$) {
  console.warn("remix:hmr: React Fast Refresh only works when the Remix compiler is running in development mode.");
} else {
  prevRefreshReg = window.$RefreshReg$;
  prevRefreshSig = window.$RefreshSig$;
  window.$RefreshReg$ = (type, id) => {
    window.$RefreshRuntime$.register(type, '"app/routes/app.additional.jsx"' + id);
  };
  window.$RefreshSig$ = window.$RefreshRuntime$.createSignatureFunctionForTransform;
}
var prevRefreshReg;
var prevRefreshSig;
var _s = $RefreshSig$();
if (import.meta) {
  import.meta.hot = createHotContext(
    //@ts-expect-error
    "app/routes/app.additional.jsx"
  );
}
var links = () => [{
  rel: "stylesheet",
  href: app_additional_default
}];
function AdditionalPage() {
  _s();
  const [data, setData] = (0, import_react.useState)([]);
  (0, import_react.useEffect)(() => {
    getListingData();
  }, []);
  const getListingData = async () => {
    console.log("getListingData======================");
    fetch("http://localhost:3001/api/order/listings", {
      method: "GET"
    }).then((response) => {
      response.json().then((res) => {
        setData(res.items);
        console.log(res, "RESSSSSSSSSSSSSSSS");
      });
    }).catch((err) => console.log(err));
  };
  console.log("Response Listing Data", data);
  return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "my-listing-page-container", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("ui-title-bar", { title: "My Listing" }, void 0, false, {
      fileName: "app/routes/app.additional.jsx",
      lineNumber: 63,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("table", { className: "my-listing-layout", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("thead", { className: "my-listing-layout-thead", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("tr", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Order ID" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 67,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Shipment Title" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 68,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Pickup" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 69,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Delivery" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 70,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Customer" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 71,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Price" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 73,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("th", { children: "Status" }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 74,
          columnNumber: 15
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 66,
        columnNumber: 13
      }, this) }, void 0, false, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 65,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("tbody", { className: "my-listing-layout-tbody", children: data.map((item) => {
        return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(import_jsx_dev_runtime.Fragment, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("tr", { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: item == null ? void 0 : item.order_id }, void 0, false, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 81,
            columnNumber: 21
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: item == null ? void 0 : item.title }, void 0, false, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 82,
            columnNumber: 21
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: [
            item.pick_up_address,
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: [
              "Pickup: ",
              item.pick_up_date
            ] }, void 0, true, {
              fileName: "app/routes/app.additional.jsx",
              lineNumber: 85,
              columnNumber: 23
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 83,
            columnNumber: 21
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: [
            item.delivery_address,
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: [
              "Delivery: ",
              item.delivery_date
            ] }, void 0, true, {
              fileName: "app/routes/app.additional.jsx",
              lineNumber: 89,
              columnNumber: 23
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 87,
            columnNumber: 21
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: [
            item.customer_name,
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: item.mobile_no }, void 0, false, {
              fileName: "app/routes/app.additional.jsx",
              lineNumber: 93,
              columnNumber: 23
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 91,
            columnNumber: 21
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: item.price }, void 0, false, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 95,
            columnNumber: 21
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("td", { children: item.status }, void 0, false, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 96,
            columnNumber: 21
          }, this)
        ] }, item == null ? void 0 : item.order_id, true, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 80,
          columnNumber: 19
        }, this) }, void 0, false, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 79,
          columnNumber: 20
        }, this);
      }) }, void 0, false, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 77,
        columnNumber: 11
      }, this)
    ] }, void 0, true, {
      fileName: "app/routes/app.additional.jsx",
      lineNumber: 64,
      columnNumber: 9
    }, this)
  ] }, void 0, true, {
    fileName: "app/routes/app.additional.jsx",
    lineNumber: 62,
    columnNumber: 7
  }, this) }, void 0, false, {
    fileName: "app/routes/app.additional.jsx",
    lineNumber: 61,
    columnNumber: 10
  }, this);
}
_s(AdditionalPage, "IEMTtLVFIuToo7X/raQbJAxzNQU=");
_c = AdditionalPage;
var _c;
$RefreshReg$(_c, "AdditionalPage");
window.$RefreshReg$ = prevRefreshReg;
window.$RefreshSig$ = prevRefreshSig;
export {
  AdditionalPage as default,
  links
};
//# sourceMappingURL=/build/routes/app.additional-MYXOXMTA.js.map
