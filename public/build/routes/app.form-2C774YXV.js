import {
  Card,
  Layout,
  Page,
  Text,
  VerticalStack,
  init_esm
} from "/build/_shared/chunk-RYZ2NSIR.js";
import "/build/_shared/chunk-HEETWZQQ.js";
import "/build/_shared/chunk-GIAAE3CH.js";
import {
  require_jsx_dev_runtime
} from "/build/_shared/chunk-XU7DNSPJ.js";
import "/build/_shared/chunk-BOXFZXVX.js";
import {
  createHotContext,
  init_remix_hmr
} from "/build/_shared/chunk-BYNBTF7W.js";
import "/build/_shared/chunk-UWV35TSL.js";
import {
  __toESM
} from "/build/_shared/chunk-PNG5AS42.js";

// app/routes/app.form.jsx
init_remix_hmr();
init_esm();
var import_jsx_dev_runtime = __toESM(require_jsx_dev_runtime());
if (!window.$RefreshReg$ || !window.$RefreshSig$ || !window.$RefreshRuntime$) {
  console.warn("remix:hmr: React Fast Refresh only works when the Remix compiler is running in development mode.");
} else {
  prevRefreshReg = window.$RefreshReg$;
  prevRefreshSig = window.$RefreshSig$;
  window.$RefreshReg$ = (type, id) => {
    window.$RefreshRuntime$.register(type, '"app/routes/app.form.jsx"' + id);
  };
  window.$RefreshSig$ = window.$RefreshRuntime$.createSignatureFunctionForTransform;
}
var prevRefreshReg;
var prevRefreshSig;
if (import.meta) {
  import.meta.hot = createHotContext(
    //@ts-expect-error
    "app/routes/app.form.jsx"
  );
  import.meta.hot.lastModified = "1699530164787.655";
}
function AdditionalPage() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Page, { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("ui-title-bar", { title: "NEW DELIVERY" }, void 0, false, {
      fileName: "app/routes/app.form.jsx",
      lineNumber: 29,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Layout, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Layout.Section, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(VerticalStack, { gap: "3", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "header-wrapper", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "Left-menu", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h3", { children: "CREATE LISTING" }, void 0, false, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 36,
            columnNumber: 19
          }, this) }, void 0, false, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 35,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "right-menu", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h3", { children: "IMPORT FROM" }, void 0, false, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 39,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", "aria-label": "eBay", role: "img", viewBox: "0 0 512 512", width: "64px", height: "64px", fill: "#000000", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("g", { id: "SVGRepo_bgCarrier", "stroke-width": "0" }, void 0, false, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 41,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("g", { id: "SVGRepo_tracerCarrier", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 42,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("g", { id: "SVGRepo_iconCarrier", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("rect", { width: "512", height: "512", rx: "15%", fill: "#ffffff" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 44,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { fill: "#e53238", d: "M98 208c-30 0-55 12-55 51 0 31 17 49 56 49 46 0 49-30 49-30h-23s-4 17-28 17c-19 0-32-13-32-31h85v-12c0-17-11-44-52-44zm-1 14c18 0 30 11 30 28H65c0-18 16-28 32-28z" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 45,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { fill: "#0064d2", d: "M150 171v119l-1 16h22l1-14s10 17 39 17c30 0 50-21 50-51 0-28-18-50-50-50-31 0-39 16-39 16v-53zm55 52c21 0 33 15 33 35 0 22-15 36-33 36-22 0-33-17-33-35s10-36 33-36z" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 46,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { fill: "#f5af03", d: "M314 208c-46 0-48 25-48 28h22s2-14 24-14c31 0 27 24 27 24h-27c-35 0-53 11-53 31s17 32 40 32c32 0 41-17 41-17l1 14h20v-62c0-30-25-36-47-36zm25 52s4 35-35 35c-16 0-23-8-23-18 0-17 24-17 58-17z" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 47,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { fill: "#86b817", d: "M348 212h25l37 72 36-72h23l-65 129h-24l18-36z" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 48,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 43,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 40,
              columnNumber: 19
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 38,
            columnNumber: 17
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 34,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "form-wrapper", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("form", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "main-form-wrapper", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "first-type", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "furniture", children: "FURNITURE TYPE" }, void 0, false, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 57,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("input", { type: "text", placeholder: "What type of furniture? i.e. couch, chair", id: "furniture", name: "furniture" }, void 0, false, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 58,
                columnNumber: 23
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 56,
              columnNumber: 21
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "form-section", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-1", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "length", children: "LENGTH" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 62,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "inputDiv-length", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("input", { type: "text", id: "length", name: "length" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 64,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 63,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 61,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-2", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "length", children: "WIDTH" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 68,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "inputDiv-width", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("input", { type: "text", id: "width", name: "width" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 70,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 69,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 67,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-3", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "length", children: "HEIGHT" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 74,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "inputDiv-height", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("input", { type: "text", id: "height", name: "height" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 76,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 75,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 73,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-4", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "forDropDown" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 80,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "dropDown-wrapper", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("select", { name: "mesure", id: "mesure", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("option", { value: "in", children: "in" }, void 0, false, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 83,
                    columnNumber: 29
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("option", { value: "cm", selected: true, children: "cm" }, void 0, false, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 84,
                    columnNumber: 29
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 82,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 81,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 79,
                columnNumber: 23
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 60,
              columnNumber: 21
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "form-section b", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-5", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "length", children: "WEIGHT" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 91,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "inputDiv-height", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("input", { type: "text", id: "weight", name: "weight" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 93,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 92,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 90,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-6", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "forDropDown" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 97,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "dropDown-wrapper", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("select", { name: "weight", id: "weight", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("option", { value: "lbs", selected: true, children: "lbs" }, void 0, false, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 100,
                    columnNumber: 29
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("option", { value: "lm", children: "km" }, void 0, false, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 101,
                    columnNumber: 29
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 99,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 98,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 96,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "flex-div-6", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("label", { htmlFor: "length", children: "QUANTITY" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 106,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "inputDiv-height", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("input", { type: "text", id: "qty", name: "qty" }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 108,
                  columnNumber: 27
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 107,
                  columnNumber: 25
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 105,
                columnNumber: 23
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 89,
              columnNumber: 21
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 55,
            columnNumber: 19
          }, this) }, void 0, false, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 54,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("aside", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "side-content", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "side-main-content", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "content-title", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("b", { children: "Include Packaging" }, void 0, false, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 117,
              columnNumber: 52
            }, this) }, void 0, false, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 117,
              columnNumber: 23
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "content-area", children: "Try to include packaging materials when measuring dimensions. (best estimate)" }, void 0, false, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 118,
              columnNumber: 23
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 116,
            columnNumber: 21
          }, this) }, void 0, false, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 115,
            columnNumber: 19
          }, this) }, void 0, false, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 114,
            columnNumber: 17
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 53,
          columnNumber: 15
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 33,
        columnNumber: 13
      }, this) }, void 0, false, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 32,
        columnNumber: 11
      }, this) }, void 0, false, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 31,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Layout.Section, { secondary: true, children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(VerticalStack, { gap: "2", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Text, { as: "h2", variant: "headingMd", children: "Details Matter" }, void 0, false, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 129,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Text, { as: "p", children: "The quotes you get are only as accurate as your listing. Make it as detailed as possible to avoid delays, price increases, and cancellations." }, void 0, false, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 132,
          columnNumber: 15
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 128,
        columnNumber: 13
      }, this) }, void 0, false, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 127,
        columnNumber: 11
      }, this) }, void 0, false, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 126,
        columnNumber: 9
      }, this)
    ] }, void 0, true, {
      fileName: "app/routes/app.form.jsx",
      lineNumber: 30,
      columnNumber: 7
    }, this)
  ] }, void 0, true, {
    fileName: "app/routes/app.form.jsx",
    lineNumber: 28,
    columnNumber: 10
  }, this);
}
_c = AdditionalPage;
var _c;
$RefreshReg$(_c, "AdditionalPage");
window.$RefreshReg$ = prevRefreshReg;
window.$RefreshSig$ = prevRefreshSig;
export {
  AdditionalPage as default
};
//# sourceMappingURL=/build/routes/app.form-2C774YXV.js.map
