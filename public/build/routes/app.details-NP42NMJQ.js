import {
  Card,
  Page,
  Text,
  init_esm
} from "/build/_shared/chunk-RYZ2NSIR.js";
import "/build/_shared/chunk-HEETWZQQ.js";
import "/build/_shared/chunk-GIAAE3CH.js";
import {
  require_jsx_dev_runtime
} from "/build/_shared/chunk-XU7DNSPJ.js";
import "/build/_shared/chunk-BOXFZXVX.js";
import {
  createHotContext,
  init_remix_hmr
} from "/build/_shared/chunk-BYNBTF7W.js";
import "/build/_shared/chunk-UWV35TSL.js";
import {
  __toESM
} from "/build/_shared/chunk-PNG5AS42.js";

// app/routes/app.details.jsx
init_remix_hmr();
init_esm();

// app/routes/assets/White_Box.jpg
var White_Box_default = "/build/_assets/White_Box-JVYMVTK7.jpg";

// app/routes/assets/details_page.css
var details_page_default = "/build/_assets/details_page-VARWTBO6.css";

// app/routes/app.details.jsx
var import_jsx_dev_runtime = __toESM(require_jsx_dev_runtime());
if (!window.$RefreshReg$ || !window.$RefreshSig$ || !window.$RefreshRuntime$) {
  console.warn("remix:hmr: React Fast Refresh only works when the Remix compiler is running in development mode.");
} else {
  prevRefreshReg = window.$RefreshReg$;
  prevRefreshSig = window.$RefreshSig$;
  window.$RefreshReg$ = (type, id) => {
    window.$RefreshRuntime$.register(type, '"app/routes/app.details.jsx"' + id);
  };
  window.$RefreshSig$ = window.$RefreshRuntime$.createSignatureFunctionForTransform;
}
var prevRefreshReg;
var prevRefreshSig;
if (import.meta) {
  import.meta.hot = createHotContext(
    //@ts-expect-error
    "app/routes/app.details.jsx"
  );
}
var links = () => [{
  rel: "stylesheet",
  href: details_page_default
}];
function Details() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("ui-title-bar", { title: "Shipment Details" }, void 0, false, {
      fileName: "app/routes/app.details.jsx",
      lineNumber: 41,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "first-conatiner", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { roundedAbove: "md", background: "bg-surface-secondary", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: "Your shipment is now listed on uShip with an offer price of $150.00 We will notify you when a service provider accepts your offer price, or if there are questions your shipment. You'll have 24 hours or less to when a service providers accepts your price." }, void 0, false, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 46,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("a", { href: "/path", className: "blue", children: "Go to uShip \u22D9" }, void 0, false, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 53,
          columnNumber: 15
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 45,
        columnNumber: 13
      }, this) }, void 0, false, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 44,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-container", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-container-wrapper", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { roundedAbove: "md", background: "bg-surface-secondary", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Text, { as: "h2", variant: "bodyMd", children: "Shipment Listing Information" }, void 0, false, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 61,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-inside-container", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-image", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("img", { src: White_Box_default, alt: "Logo" }, void 0, false, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 66,
              columnNumber: 21
            }, this) }, void 0, false, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 65,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-table-first", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-table-first-wrapper", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-wrapper-container", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " Shipment ID:" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 72,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "740152872" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 73,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 71,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " Shopify ID:" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 76,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "AH642578SD" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 77,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 75,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " Offer:" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 80,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "$170" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 81,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 79,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 70,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-wrapper-container", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "status-shipment product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "status", children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " Status:" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 87,
                        columnNumber: 29
                      }, this),
                      " ",
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "blue", children: "Active" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 88,
                        columnNumber: 29
                      }, this)
                    ] }, void 0, true, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 86,
                      columnNumber: 27
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "shipment-track", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("a", { href: "/path", children: "Track Shipment" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 92,
                      columnNumber: 29
                    }, this) }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 90,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 85,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " Shipment Title:" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 96,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "5 items for transaction to torronto" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 97,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 95,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: "Date Listed:" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 100,
                      columnNumber: 27
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "10/23/2023 7:08 AM CST" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 101,
                      columnNumber: 27
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " End Date:" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 102,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "10/26/2023 11:59 PM CST" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 103,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 99,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 84,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 69,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-table-second", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: "Customer:" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 109,
                    columnNumber: 25
                  }, this),
                  " ",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "Jason Henry" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 110,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 108,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "top", children: [
                  "Contact: (316) 555-0116 ",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("br", {}, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 113,
                    columnNumber: 49
                  }, this),
                  "Mail:",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "blue", children: " jason.h-120@gmail.com" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 115,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 112,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "top", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: " uShip Listing:" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 118,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "blue", children: [
                    "www.uship.com/shipment/7401528...",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "link-svg", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", width: "16", height: "16", viewBox: "0 0 16 16", fill: "none", children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M12 8.66667V12.6667C12 13.0203 11.8595 13.3594 11.6095 13.6095C11.3594 13.8595 11.0203 14 10.6667 14H3.33333C2.97971 14 2.64057 13.8595 2.39052 13.6095C2.14048 13.3594 2 13.0203 2 12.6667V5.33333C2 4.97971 2.14048 4.64057 2.39052 4.39052C2.64057 4.14048 2.97971 4 3.33333 4H7.33333", stroke: "#0083BB", "stroke-width": "1.33333", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 124,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M10 2H14V6", stroke: "#0083BB", "stroke-width": "1.33333", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 126,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M6.66675 9.33333L14.0001 2", stroke: "#0083BB", "stroke-width": "1.33333", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 128,
                        columnNumber: 31
                      }, this)
                    ] }, void 0, true, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 122,
                      columnNumber: 29
                    }, this) }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 121,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 119,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 117,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "top", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: "Carrier Contact: ---" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 135,
                  columnNumber: 25
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 134,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 107,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 68,
              columnNumber: 19
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 64,
            columnNumber: 17
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 60,
          columnNumber: 15
        }, this) }, void 0, false, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 59,
          columnNumber: 13
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "middle-container-aside", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Text, { as: "h2", variant: "bodyMd", children: "Origin, Destination, & Route Information" }, void 0, false, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 146,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "address-tab-wrapper", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "address-tab", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "data-tab", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "info-orgin-wrapper", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "way-svg", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", width: "20", height: "20", viewBox: "0 0 20 20", fill: "none", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("g", { "clip-path": "url(#clip0_218_101)", children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M1.66675 10H4.16675", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 157,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M15.8333 10H18.3333", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 159,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M10 1.66666V4.16666", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 161,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M10 15.8333V18.3333", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 163,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M10.0001 15.8333C13.2217 15.8333 15.8334 13.2217 15.8334 10C15.8334 6.77834 13.2217 4.16666 10.0001 4.16666C6.77842 4.16666 4.16675 6.77834 4.16675 10C4.16675 13.2217 6.77842 15.8333 10.0001 15.8333Z", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 165,
                        columnNumber: 31
                      }, this),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M10 12.5C11.3807 12.5 12.5 11.3807 12.5 10C12.5 8.61929 11.3807 7.5 10 7.5C8.61929 7.5 7.5 8.61929 7.5 10C7.5 11.3807 8.61929 12.5 10 12.5Z", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 167,
                        columnNumber: 31
                      }, this)
                    ] }, void 0, true, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 156,
                      columnNumber: 29
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("defs", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("clipPath", { id: "clip0_218_101", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("rect", { width: "20", height: "20", fill: "white" }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 172,
                      columnNumber: 33
                    }, this) }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 171,
                      columnNumber: 31
                    }, this) }, void 0, false, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 170,
                      columnNumber: 29
                    }, this)
                  ] }, void 0, true, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 154,
                    columnNumber: 27
                  }, this) }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 153,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h3", { className: "product-content", children: "ORIGIN:" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 177,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 152,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("a", { href: "/path", className: "blue", children: "Edit" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 182,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 151,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: "600 Garrison CT APT 201 New Castle, Delware 19720 Pickup: On 10/23/2023" }, void 0, false, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 186,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 150,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "address-tab", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "data-tab-data", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "way-svg", children: [
                  " ",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", width: "20", height: "20", viewBox: "0 0 20 20", fill: "none", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M2.5 9.16667L18.3333 1.66667L10.8333 17.5L9.16667 10.8333L2.5 9.16667Z", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 197,
                    columnNumber: 27
                  }, this) }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 195,
                    columnNumber: 25
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 193,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h3", { className: "product-content", children: "ROUTE" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 201,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 192,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: "Est. Distance: 122.00 Miles" }, void 0, false, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 203,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 191,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "address-tab", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "data-tab-data", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { className: "way-svg", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", width: "20", height: "20", viewBox: "0 0 20 20", fill: "none", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M16.6666 8.33333C16.6666 13.3333 9.99992 18.3333 9.99992 18.3333C9.99992 18.3333 3.33325 13.3333 3.33325 8.33333C3.33325 6.56522 4.03563 4.86953 5.28587 3.61929C6.53612 2.36905 8.23181 1.66667 9.99992 1.66667C11.768 1.66667 13.4637 2.36905 14.714 3.61929C15.9642 4.86953 16.6666 6.56522 16.6666 8.33333Z", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 210,
                    columnNumber: 27
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("path", { d: "M10 10.8333C11.3807 10.8333 12.5 9.71404 12.5 8.33333C12.5 6.95262 11.3807 5.83333 10 5.83333C8.61929 5.83333 7.5 6.95262 7.5 8.33333C7.5 9.71404 8.61929 10.8333 10 10.8333Z", stroke: "black", "stroke-width": "1.66667", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, false, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 212,
                    columnNumber: 27
                  }, this)
                ] }, void 0, true, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 208,
                  columnNumber: 25
                }, this) }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 207,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h3", { className: "product-content", children: "DESTINATION" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 216,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 206,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { children: "584 Settlers Lane New York, NY 10007 Delivery: Before 10/30/2023" }, void 0, false, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 218,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 205,
              columnNumber: 19
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 149,
            columnNumber: 17
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 145,
          columnNumber: 15
        }, this) }, void 0, false, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 144,
          columnNumber: 13
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 58,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "last-conatiner", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Card, { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(Text, { as: "h2", variant: "bodyMd", children: "Household Goods - Furniture" }, void 0, false, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 229,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "household-wrapper", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "household-product-details border-bottom", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("h2", { className: "product-content", children: "5 ITEMS FOR TRANSPORT " }, void 0, false, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 235,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Total furniture items: ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "5" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 237,
                  columnNumber: 46
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 236,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 234,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Total Weight: ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "286.6 lbs" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 242,
                  columnNumber: 37
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 241,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Palletized: ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "No" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 245,
                  columnNumber: 35
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 244,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 240,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Crated:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "No" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 250,
                  columnNumber: 30
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 249,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Stackable:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "No" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 253,
                  columnNumber: 33
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 252,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 248,
              columnNumber: 19
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 233,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "household-product-details border-bottom", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
              "Item 1: ",
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "Dining Table" }, void 0, false, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 260,
                columnNumber: 31
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 259,
              columnNumber: 21
            }, this) }, void 0, false, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 258,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Quantity:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "1" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 265,
                  columnNumber: 32
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 264,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Dimensions:",
                " ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "L: 3 ft 11 in. W: 2 ft 11 in. H: 2 ft 0 in." }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 269,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 267,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Weight:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "110.2 lbs" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 272,
                  columnNumber: 30
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 271,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 263,
              columnNumber: 19
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 257,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "household-product-details", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
              "Item 1: ",
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "Dining Table" }, void 0, false, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 279,
                columnNumber: 31
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 278,
              columnNumber: 21
            }, this) }, void 0, false, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 277,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Quantity:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "1" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 284,
                  columnNumber: 32
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 283,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Dimensions:",
                " ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "L: 3 ft 11 in. W: 2 ft 11 in. H: 2 ft 0 in." }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 288,
                  columnNumber: 23
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 286,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("p", { className: "product-content", children: [
                "Weight:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)("span", { children: "110.2 lbs" }, void 0, false, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 291,
                  columnNumber: 30
                }, this)
              ] }, void 0, true, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 290,
                columnNumber: 21
              }, this)
            ] }, void 0, true, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 282,
              columnNumber: 19
            }, this)
          ] }, void 0, true, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 276,
            columnNumber: 17
          }, this)
        ] }, void 0, true, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 232,
          columnNumber: 15
        }, this)
      ] }, void 0, true, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 228,
        columnNumber: 13
      }, this) }, void 0, false, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 227,
        columnNumber: 11
      }, this)
    ] }, void 0, true, {
      fileName: "app/routes/app.details.jsx",
      lineNumber: 43,
      columnNumber: 9
    }, this) }, void 0, false, {
      fileName: "app/routes/app.details.jsx",
      lineNumber: 42,
      columnNumber: 7
    }, this)
  ] }, void 0, true, {
    fileName: "app/routes/app.details.jsx",
    lineNumber: 40,
    columnNumber: 10
  }, this);
}
_c = Details;
var _c;
$RefreshReg$(_c, "Details");
window.$RefreshReg$ = prevRefreshReg;
window.$RefreshSig$ = prevRefreshSig;
export {
  Details as default,
  links
};
//# sourceMappingURL=/build/routes/app.details-NP42NMJQ.js.map
