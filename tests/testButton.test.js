import "@testing-library/jest-dom"; // Importing jest-dom extends expect with matchers
import React from "react";
import { render, screen } from "@testing-library/react";
import { describe, test, expect } from "@jest/globals"; // or import directly from 'jest' without the globals import
import Button from "../app/routes/test.jsx"; // Adjust the relative path as needed

describe("Button Component", () => {
  test("renders button with correct text", () => {
    render(<Button text="Click me" />);
    const buttonElement = screen.getByText("Click me");
    // expect(buttonElement).toBeTruthy();
    expect(buttonElement).toBeInTheDocument();
  });
});
