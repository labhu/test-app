import { useCallback, useEffect, useState } from "react";
import app_indexStyles from "./assets/styles.css";
import { json } from "@remix-run/node";
import { useActionData } from "@remix-run/react";
import {
  Page,
  Layout,
  Text,
  VerticalStack,
  Card,
  Button,
  FormLayout,
  TextField,
  Form,
} from "@shopify/polaris";
import { Logo } from "./assets/logo";
// import axios from "axios";

import { authenticate } from "../shopify.server";
export const links = () => [{ rel: "stylesheet", href: app_indexStyles }];

export const loader = async ({ request }) => {
  await authenticate.admin(request);

  return null;
};

export async function action({ request }) {
  const { admin } = await authenticate.admin(request);

  const color = ["Red", "Orange", "Yellow", "Green"][
    Math.floor(Math.random() * 4)
  ];
  const response = await admin.graphql(
    `#graphql
      mutation populateProduct($input: ProductInput!) {
        productCreate(input: $input) {
          product {
            id
            title
            handle
            status
            variants(first: 10) {
              edges {
                node {
                  id
                  price
                  barcode
                  createdAt
                }
              }
            }
          }
        }
      }`,
    {
      variables: {
        input: {
          title: `${color} Snowboard`,
          variants: [{ price: Math.random() * 100 }],
        },
      },
    }
  );

  const responseJson = await response.json();

  return json({
    product: responseJson.data.productCreate.product,
  });
}

export default function Index() {
  const [formState, setformState] = useState({
    email: "",
    password: "",
    unique_key: "",
    access_key: "",
  });

  // const nav = useNavigation();
  const actionData = useActionData();
  // const submit = useSubmit();

  // const isLoading =
  //   ["loading", "submitting"].includes(nav.state) && nav.formMethod === "POST";

  const productId = actionData?.product?.id.replace(
    "gid://shopify/Product/",
    ""
  );

  const handleSubmit = useCallback(() => {
    // console.log("LODFJF");
    // console.log(formState.unique_key);
  }, []);

  const Openpopup = useCallback(() => {
    let get = document.querySelector("#popup");
    if (get) {
      get.classList.toggle("hide");
    }
    // if (get.style.display == "none") {
    //   get.style.display = "block";
    // } else {
    //   get.style.display = "none";
    // }
  }, []);

  useEffect(() => {
    if (productId) {
      shopify.toast.show("Product created");
    }
  }, [productId]);

  // useEffect(() => {
  //   generateProduct();
  // }, []);

  // const generateProduct = () => submit({}, { replace: true, method: "POST" });
  // console.log(formState.unique_key);
  // console.log(formState.access_key);

  const generateProduct = async () => {
    console.log(formState.email);
    console.log(formState.password);
    let headers = new Headers();

    headers.append("Content-Type", "application/json");
    headers.append("Accept", "application/json");

    // headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Access-Control-Allow-Credentials', 'true');

    // headers.append('GET', 'POST', 'OPTIONS');

    await fetch("https://api-web.ushipdev.com/oauth/token_authenticated", {
      credentials: "include",
      // headers: {
      //   "Access-Control-Allow-Origin": "*", // Replace with your allowed origin(s)
      //   "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
      //   "Access-Control-Allow-Headers":
      //     "Origin, X-Requested-With, Content-Type, Accept",
      //   // 'x-frame-options' : 'none'
      // },
      method: "POST",
      body: JSON.stringify({
        client_id: "djc82cpkrkcbqr3xjtdgkb4u",
        client_secret: "G5tPdQEvty",
        grant_type: "password",
        username: formState.email,
        password: formState.password,
      }),
    })
      // .then(response => response.json())
      .then((response) => console.log(response, "RESSS"))
      .catch((err) => console.error(err));

    // try {
    //   const responseData  =await axios.post(
    //     "https://api.ushipsandbox.com/partner/oauth/token_authenticated",
    //     {
    //       client_id: "djc82cpkrkcbqr3xjtdgkb4u",
    //       client_secret: "G5tPdQEvty",
    //       grant_type: "password",
    //       username: formState.email,
    //       password: formState.password,
    //     }
    //   )
    //   console.log("responseData--------------------------" , responseData);
    // } catch (e) {
    //   console.log("Error in create product::");

    // }

    setformState({
      email: "",
      password: "",
      access_key: "",
      unique_key: "",
    });
  };

  const handleChange = (field, value) => {
    setformState({ ...formState, [field]: value });
  };

  return (
    <>
      <Page>
        <VerticalStack>
          <div className="login-page-container">
            <Layout>
              <div className="login-page-layout-container">
                <Card>
                  <div className="login-page-layout-logo">
                    <Logo />
                  </div>

                  <div className="login-page-layout-text">
                    <Text as="h2" variant="headingMd">
                      Access Uship Listing
                    </Text>
                  </div>

                  <Form onSubmit={handleSubmit}>
                    <FormLayout>
                      <TextField
                        label="Access Key"
                        type="text"
                        autoComplete="none"
                        placeholder="Enter your access key here"
                        name="username"
                        value={formState.access_key}
                        onChange={(value) => handleChange("access_key", value)}
                        requiredIndicator
                      />

                      <div className="Submit-Button">
                        <Button primary fullWidth onClick={generateProduct}>
                          Submit
                        </Button>
                      </div>
                      <div className="Forget-Button">
                        <Button plain fullWidth onClick={Openpopup}>
                          How to get access key?
                        </Button>
                        {/* <a
                          href="https://login.uship.com/signin/forgot-password"
                          target="_blank"
                        >
                          How to get access key?
                        </a> */}
                      </div>
                    </FormLayout>
                  </Form>
                </Card>
                <div className="Access-key-popup hide" id="popup">
                  <Card>
                    <div>
                      <Text as="h2" variant="headingMd">
                        Where to find Access key?
                      </Text>
                      <ul>
                        <li>
                          Login to{" "}
                          <a
                            href="https://login.ushipsandbox.com"
                            className="Access-key-popup-A"
                            target="_blank"
                            rel="noreferrer"
                          >
                            https://login.ushipsandbox.com/
                          </a>
                        </li>
                        <li>Go to (Menu-Name)(Sub Menu-Name)</li>
                        <li>Click on API KEY GENERATION</li>
                      </ul>
                      <p>
                        After following above steps you will be able to see
                        below screen and can generate your unique key from here.
                        This key can be used to access your uship account in
                        shopify.
                      </p>
                    </div>
                    <div className="Access-key-popup-form">
                      <Card>
                        <div className="Api-form">
                          <Text as="h2" variant="headingMd">
                            API KEY GENERATION
                          </Text>
                          <p>
                            Generate a unique key below. Once generated, you can
                            copuy/paste your key into your application. Note
                            that generating a new key will invalidate a current
                            existingkey . <b>Learn More</b>
                          </p>
                          <form>
                            <div className="Generate-key-Button">
                              <Button onClick={handleSubmit}>
                                Generate Key
                              </Button>
                            </div>
                            <div className="Api-form-input-box">
                              <TextField
                                label="YOUR UNIQUE KEY"
                                type="text"
                                autoComplete="none"
                                placeholder="Enter your unique key here"
                                name="username"
                                value={formState.unique_key}
                                onChange={(value) =>
                                  handleChange("unique_key", value)
                                }
                                requiredIndicator
                              />
                              <Button onClick={handleSubmit}>✔</Button>
                            </div>
                          </form>
                        </div>
                      </Card>
                    </div>
                  </Card>
                </div>
              </div>
            </Layout>
          </div>
        </VerticalStack>
      </Page>
    </>
  );
}
