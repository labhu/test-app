import {
  Page,
  // Layout,
  Text,
  // VerticalStack,
  Card,
  // Button,
  // FormLayout,
  // TextField,
  // Form,
} from "@shopify/polaris";
// import {} from "./assets/White Box.jpg";
import ProductImg from "./assets/White_Box.jpg";
// import ProductImg from "./assets/White_Box.svg";

import style from "./assets/details_page.css";
export const links = () => [{ rel: "stylesheet", href: style }];

export default function Details() {
  return (
    <div>
      <ui-title-bar title="Shipment Details" />
      <Page>
        <Card>
          <div className="first-conatiner">
            <Card roundedAbove="md" background="bg-surface-secondary">
              <p>
                Your shipment is now listed on uShip with an offer price of
                $150.00 We will notify you when a service provider accepts your
                offer price, or if there are questions your shipment. You'll
                have 24 hours or less to when a service providers accepts your
                price.
              </p>
              <a href="/path" className="blue">
                Go to uShip &#8921;
              </a>
            </Card>
          </div>
          <div className="middle-container">
            <div className="middle-container-wrapper">
              <Card roundedAbove="md" background="bg-surface-secondary">
                <Text as="h2" variant="bodyMd">
                  Shipment Listing Information
                </Text>
                <div className="middle-inside-container">
                  <div className="middle-image">
                    <img src={ProductImg} alt="Logo" />
                  </div>
                  <div className="middle-table-first">
                    <div className="middle-table-first-wrapper">
                      <div className="middle-wrapper-container">
                        <div className="product-label-data">
                          <h2 className="product-content"> Shipment ID:</h2>{" "}
                          <span>740152872</span>
                        </div>
                        <div className="product-label-data">
                          <h2 className="product-content"> Shopify ID:</h2>{" "}
                          <span>AH642578SD</span>
                        </div>
                        <div className="product-label-data">
                          <h2 className="product-content"> Offer:</h2>{" "}
                          <span>$170</span>
                        </div>
                      </div>
                      <div className="middle-wrapper-container">
                        <div className="status-shipment product-label-data">
                          <div className="status">
                            <h2 className="product-content"> Status:</h2>{" "}
                            <span className="blue">Active</span>
                          </div>
                          <div className="shipment-track">
                            {/* <a href="">Track Shipment</a> */}
                            <a href="/path">Track Shipment</a>
                          </div>
                        </div>
                        <div className="product-label-data">
                          <h2 className="product-content"> Shipment Title:</h2>{" "}
                          <span>5 items for transaction to torronto</span>
                        </div>
                        <div className="product-label-data">
                          <h2 className="product-content">Date Listed:</h2>
                          <span>10/23/2023 7:08 AM CST</span>
                          <h2 className="product-content"> End Date:</h2>{" "}
                          <span>10/26/2023 11:59 PM CST</span>
                        </div>
                      </div>
                    </div>
                    <div className="middle-table-second">
                      <div>
                        <h2 className="product-content">Customer:</h2>{" "}
                        <span>Jason Henry</span>
                      </div>
                      <div className="top">
                        Contact: (316) 555-0116 <br />
                        Mail:
                        <span className="blue"> jason.h-120@gmail.com</span>
                      </div>
                      <div className="top">
                        <h2 className="product-content"> uShip Listing:</h2>
                        <span className="blue">
                          www.uship.com/shipment/7401528...
                          <span className="link-svg">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              fill="none"
                            >
                              <path
                                d="M12 8.66667V12.6667C12 13.0203 11.8595 13.3594 11.6095 13.6095C11.3594 13.8595 11.0203 14 10.6667 14H3.33333C2.97971 14 2.64057 13.8595 2.39052 13.6095C2.14048 13.3594 2 13.0203 2 12.6667V5.33333C2 4.97971 2.14048 4.64057 2.39052 4.39052C2.64057 4.14048 2.97971 4 3.33333 4H7.33333"
                                stroke="#0083BB"
                                stroke-width="1.33333"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M10 2H14V6"
                                stroke="#0083BB"
                                stroke-width="1.33333"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M6.66675 9.33333L14.0001 2"
                                stroke="#0083BB"
                                stroke-width="1.33333"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                            </svg>
                          </span>
                        </span>
                      </div>
                      <div className="top">
                        <h2 className="product-content">
                          Carrier Contact: ---
                        </h2>
                      </div>
                    </div>
                  </div>
                </div>
              </Card>
            </div>
            <div className="middle-container-aside">
              <Card>
                <Text as="h2" variant="bodyMd">
                  Origin, Destination, & Route Information
                </Text>
                <div className="address-tab-wrapper">
                  <div className="address-tab">
                    <div className="data-tab">
                      <div className="info-orgin-wrapper">
                        <span className="way-svg">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 20 20"
                            fill="none"
                          >
                            <g clip-path="url(#clip0_218_101)">
                              <path
                                d="M1.66675 10H4.16675"
                                stroke="black"
                                stroke-width="1.66667"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M15.8333 10H18.3333"
                                stroke="black"
                                stroke-width="1.66667"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M10 1.66666V4.16666"
                                stroke="black"
                                stroke-width="1.66667"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M10 15.8333V18.3333"
                                stroke="black"
                                stroke-width="1.66667"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M10.0001 15.8333C13.2217 15.8333 15.8334 13.2217 15.8334 10C15.8334 6.77834 13.2217 4.16666 10.0001 4.16666C6.77842 4.16666 4.16675 6.77834 4.16675 10C4.16675 13.2217 6.77842 15.8333 10.0001 15.8333Z"
                                stroke="black"
                                stroke-width="1.66667"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                              <path
                                d="M10 12.5C11.3807 12.5 12.5 11.3807 12.5 10C12.5 8.61929 11.3807 7.5 10 7.5C8.61929 7.5 7.5 8.61929 7.5 10C7.5 11.3807 8.61929 12.5 10 12.5Z"
                                stroke="black"
                                stroke-width="1.66667"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                              />
                            </g>
                            <defs>
                              <clipPath id="clip0_218_101">
                                <rect width="20" height="20" fill="white" />
                              </clipPath>
                            </defs>
                          </svg>
                        </span>
                        <h3 className="product-content">ORIGIN:</h3>
                      </div>
                      {/* <a href="#" className="blue">
                        Edit
                      </a> */}
                      <a href="/path" className="blue">
                        Edit
                      </a>
                    </div>
                    <p>
                      600 Garrison CT APT 201 New Castle, Delware 19720 Pickup:
                      On 10/23/2023
                    </p>
                  </div>
                  <div className="address-tab">
                    <div className="data-tab-data">
                      <span className="way-svg">
                        {" "}
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                        >
                          <path
                            d="M2.5 9.16667L18.3333 1.66667L10.8333 17.5L9.16667 10.8333L2.5 9.16667Z"
                            stroke="black"
                            stroke-width="1.66667"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                        </svg>
                      </span>
                      <h3 className="product-content">ROUTE</h3>
                    </div>
                    <p>Est. Distance: 122.00 Miles</p>
                  </div>
                  <div className="address-tab">
                    <div className="data-tab-data">
                      <span className="way-svg">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                        >
                          <path
                            d="M16.6666 8.33333C16.6666 13.3333 9.99992 18.3333 9.99992 18.3333C9.99992 18.3333 3.33325 13.3333 3.33325 8.33333C3.33325 6.56522 4.03563 4.86953 5.28587 3.61929C6.53612 2.36905 8.23181 1.66667 9.99992 1.66667C11.768 1.66667 13.4637 2.36905 14.714 3.61929C15.9642 4.86953 16.6666 6.56522 16.6666 8.33333Z"
                            stroke="black"
                            stroke-width="1.66667"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                          <path
                            d="M10 10.8333C11.3807 10.8333 12.5 9.71404 12.5 8.33333C12.5 6.95262 11.3807 5.83333 10 5.83333C8.61929 5.83333 7.5 6.95262 7.5 8.33333C7.5 9.71404 8.61929 10.8333 10 10.8333Z"
                            stroke="black"
                            stroke-width="1.66667"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                        </svg>
                      </span>
                      <h3 className="product-content">DESTINATION</h3>
                    </div>
                    <p>
                      584 Settlers Lane New York, NY 10007 Delivery: Before
                      10/30/2023
                    </p>
                  </div>
                </div>
              </Card>
            </div>
          </div>
          <div className="last-conatiner">
            <Card>
              <Text as="h2" variant="bodyMd">
                Household Goods - Furniture
              </Text>
              <div className="household-wrapper">
                <div className="household-product-details border-bottom">
                  <div className="about-product-data">
                    <h2 className="product-content">5 ITEMS FOR TRANSPORT </h2>
                    <p className="product-content">
                      Total furniture items: <span>5</span>
                    </p>
                  </div>
                  <div className="about-product-data">
                    <p className="product-content">
                      Total Weight: <span>286.6 lbs</span>
                    </p>
                    <p className="product-content">
                      Palletized: <span>No</span>
                    </p>
                  </div>
                  <div className="about-product-data">
                    <p className="product-content">
                      Crated:<span>No</span>
                    </p>
                    <p className="product-content">
                      Stackable:<span>No</span>
                    </p>
                  </div>
                </div>
                <div className="household-product-details border-bottom">
                  <div className="about-product-data">
                    <p className="product-content">
                      Item 1: <span>Dining Table</span>
                    </p>
                  </div>
                  <div className="about-product-data">
                    <p className="product-content">
                      Quantity:<span>1</span>
                    </p>
                    <p className="product-content">
                      Dimensions:{" "}
                      <span>L: 3 ft 11 in. W: 2 ft 11 in. H: 2 ft 0 in.</span>
                    </p>
                    <p className="product-content">
                      Weight:<span>110.2 lbs</span>
                    </p>
                  </div>
                </div>
                <div className="household-product-details">
                  <div className="about-product-data">
                    <p className="product-content">
                      Item 1: <span>Dining Table</span>
                    </p>
                  </div>
                  <div className="about-product-data">
                    <p className="product-content">
                      Quantity:<span>1</span>
                    </p>
                    <p className="product-content">
                      Dimensions:{" "}
                      <span>L: 3 ft 11 in. W: 2 ft 11 in. H: 2 ft 0 in.</span>
                    </p>
                    <p className="product-content">
                      Weight:<span>110.2 lbs</span>
                    </p>
                  </div>
                </div>
              </div>
            </Card>
          </div>
        </Card>
      </Page>
    </div>
  );
}
