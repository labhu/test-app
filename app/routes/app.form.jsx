import {
  // Box,
  Card,
  Layout,
  // Link,
  // List,
  Page,
  Text,
  VerticalStack,
} from "@shopify/polaris";

export default function AdditionalPage() {
  return (
    <Page>
      <ui-title-bar title="NEW DELIVERY" />
      <Layout>
        <Layout.Section>
          <Card>
            <VerticalStack gap="3">
              <div className="header-wrapper">
                <div className="Left-menu">
                  <h3>CREATE LISTING</h3>
                </div>
                <div className="right-menu">
                  <h3>IMPORT FROM</h3>
                  <svg xmlns="http://www.w3.org/2000/svg" aria-label="eBay" role="img" viewBox="0 0 512 512" width="64px" height="64px" fill="#000000">
                    <g id="SVGRepo_bgCarrier" stroke-width="0" />
                    <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round" />
                    <g id="SVGRepo_iconCarrier">
                      <rect width="512" height="512" rx="15%" fill="#ffffff" />
                      <path fill="#e53238" d="M98 208c-30 0-55 12-55 51 0 31 17 49 56 49 46 0 49-30 49-30h-23s-4 17-28 17c-19 0-32-13-32-31h85v-12c0-17-11-44-52-44zm-1 14c18 0 30 11 30 28H65c0-18 16-28 32-28z" />
                      <path fill="#0064d2" d="M150 171v119l-1 16h22l1-14s10 17 39 17c30 0 50-21 50-51 0-28-18-50-50-50-31 0-39 16-39 16v-53zm55 52c21 0 33 15 33 35 0 22-15 36-33 36-22 0-33-17-33-35s10-36 33-36z" />
                      <path fill="#f5af03" d="M314 208c-46 0-48 25-48 28h22s2-14 24-14c31 0 27 24 27 24h-27c-35 0-53 11-53 31s17 32 40 32c32 0 41-17 41-17l1 14h20v-62c0-30-25-36-47-36zm25 52s4 35-35 35c-16 0-23-8-23-18 0-17 24-17 58-17z" />
                      <path fill="#86b817" d="M348 212h25l37 72 36-72h23l-65 129h-24l18-36z" />
                    </g>
                  </svg>
                </div>
              </div>
              <div className="form-wrapper">
                <form>
                  <div className="main-form-wrapper">
                    <div className="first-type">
                      <label htmlFor="furniture">FURNITURE TYPE</label>
                      <input type="text" placeholder="What type of furniture? i.e. couch, chair" id="furniture" name="furniture" />
                    </div>
                    <div className="form-section">
                      <div className="flex-div-1">
                        <label htmlFor="length">LENGTH</label>
                        <div className="inputDiv-length">
                          <input type="text" id="length" name="length" />
                        </div>
                      </div>
                      <div className="flex-div-2">
                        <label htmlFor="length">WIDTH</label>
                        <div className="inputDiv-width">
                          <input type="text" id="width" name="width" />
                        </div>
                      </div>
                      <div className="flex-div-3">
                        <label htmlFor="length">HEIGHT</label>
                        <div className="inputDiv-height">
                          <input type="text" id="height" name="height" />
                        </div>
                      </div>
                      <div className="flex-div-4">
                        <label htmlFor="forDropDown"></label>
                        <div className="dropDown-wrapper">
                          <select name="mesure" id="mesure">
                            <option value="in">in</option>
                            <option value="cm" selected>cm</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="form-section b">
                      <div className="flex-div-5">
                        <label htmlFor="length">WEIGHT</label>
                        <div className="inputDiv-height">
                          <input type="text" id="weight" name="weight" />
                        </div>
                      </div>
                      <div className="flex-div-6">
                        <label htmlFor="forDropDown"></label>
                        <div className="dropDown-wrapper">
                          <select name="weight" id="weight">
                            <option value="lbs" selected>lbs</option>
                            <option value="lm">km</option>
                          </select>
                        </div>
                      </div>
                      <div className="flex-div-6">
                        <label htmlFor="length">QUANTITY</label>
                        <div className="inputDiv-height">
                          <input type="text" id="qty" name="qty" />
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                <aside>
                  <div className="side-content">
                    <div className="side-main-content">
                      <p className="content-title"><b>Include Packaging</b></p>
                      <p className="content-area">Try to include packaging materials when measuring dimensions. (best estimate)</p>
                    </div>
                  </div>
                </aside>
              </div>
            </VerticalStack>
          </Card>
        </Layout.Section>
        <Layout.Section secondary>
          <Card>
            <VerticalStack gap="2">
              <Text as="h2" variant="headingMd">
                Details Matter
              </Text>
              <Text as="p">The quotes you get are only as accurate as your listing. Make it as detailed as possible to avoid delays, price increases, and cancellations.</Text>
              {/* <List spacing="extraTight">
                  <List.Item>
                    <Link
                      url="https://shopify.dev/docs/apps/design-guidelines/navigation#app-nav"
                      target="_blank"
                    >
                      App nav best practices
                    </Link>
                  </List.Item>
                </List> */}
            </VerticalStack>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}

// function Code({ children }) {
//   return (
//     <Box
//       as="span"
//       padding="025"
//       paddingInlineStart="1"
//       paddingInlineEnd="1"
//       background="bg-subdued"
//       borderWidth="1"
//       borderColor="border"
//       borderRadius="1"
//     >
//       <code>{children}</code>
//     </Box>
//   );
// }
