// @ts-nocheck
import {
  // Box,
  // Card,
  // Layout,
  // Link,
  // List,
  Page,
  // Text,
  // VerticalStack,
} from "@shopify/polaris";
import app_additionalStyles from "./assets/app.additional.css";
import  { useEffect, useState } from "react";
// import axios from "axios";
// import { getToken } from "./config";

export const links = () => [{ rel: "stylesheet", href: app_additionalStyles }];

export default function AdditionalPage() {
  // const [token, setToken] = useState();
  const [data, setData] = useState([]);

  useEffect(() => {
    getListingData();
  }, []);

  const getListingData = async () => {
    console.log("getListingData======================");
    // fetch("https://apisandbox.uship.com/v2/listings", {
    fetch("http://localhost:3001/api/order/listings", {
      method: "GET",
    })
      .then((response) => {
        response.json().then((res) => {
          setData(res.items);
          console.log(res, "RESSSSSSSSSSSSSSSS");
        });
      })
      .catch((err) => console.log(err));
  };
  console.log("Response Listing Data", data);
  return (
    <Page>
      <div className="my-listing-page-container">
        <ui-title-bar title="My Listing" />
        <table className="my-listing-layout">
          <thead className="my-listing-layout-thead">
            <tr>
              <th>Order ID</th>
              <th>Shipment Title</th>
              <th>Pickup</th>
              <th>Delivery</th>
              <th>Customer</th>
              {/* <th>Goods</th> */}
              <th>Price</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody className="my-listing-layout-tbody">
            {data.map((item) => {
              return (
                <>
                  <tr key={item?.order_id}>
                    <td>{item?.order_id}</td>
                    <td>{item?.title}</td>
                    <td>
                      {item.pick_up_address}
                      <p>Pickup: {item.pick_up_date}</p>
                    </td>
                    <td>
                      {item.delivery_address}
                      <p>Delivery: {item.delivery_date}</p>
                    </td>
                    <td>
                      {item.customer_name}
                      <p>{item.mobile_no}</p>
                    </td>
                    <td>{item.price}</td>
                    <td>{item.status}</td>
                    {/* <td style={{ color: getStatusColor(item.status) }}>{item.status}</td> */}
                    {/* <td>
                      <StatusIcon status={item.status} />
                    </td> */}
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      </div>
    </Page>
  );
}

// function Code({ children }) {
//   return (
//     <Box
//       as="span"
//       padding="025"
//       paddingInlineStart="1"
//       paddingInlineEnd="1"
//       background="bg-subdued"
//       borderWidth="1"
//       borderColor="border"
//       borderRadius="1"
//     >
//       <code>{children}</code>
//     </Box>
//   );
// }
