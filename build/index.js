var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf, __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: !0 });
}, __copyProps = (to, from, except, desc) => {
  if (from && typeof from == "object" || typeof from == "function")
    for (let key of __getOwnPropNames(from))
      !__hasOwnProp.call(to, key) && key !== except && __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: !0 }) : target,
  mod
)), __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: !0 }), mod);

// <stdin>
var stdin_exports = {};
__export(stdin_exports, {
  assets: () => assets_manifest_default,
  assetsBuildDirectory: () => assetsBuildDirectory,
  entry: () => entry,
  future: () => future,
  mode: () => mode,
  publicPath: () => publicPath,
  routes: () => routes
});
module.exports = __toCommonJS(stdin_exports);

// app/entry.server.jsx
var entry_server_exports = {};
__export(entry_server_exports, {
  default: () => handleRequest
});
var import_stream = require("stream"), import_server2 = require("react-dom/server"), import_react = require("@remix-run/react"), import_isbot = __toESM(require("isbot"));

// app/shopify.server.js
var import_node = require("@shopify/shopify-app-remix/adapters/node"), import_server = require("@shopify/shopify-app-remix/server"), import_shopify_app_session_storage_prisma = require("@shopify/shopify-app-session-storage-prisma"), import__ = require("@shopify/shopify-api/rest/admin/2023-07");

// app/db.server.js
var import_client = require("@prisma/client"), prisma = global.prisma || new import_client.PrismaClient();
global.prisma || (global.prisma = new import_client.PrismaClient());
var db_server_default = prisma;

// app/shopify.server.js
var shopify2 = (0, import_server.shopifyApp)({
  apiKey: process.env.SHOPIFY_API_KEY,
  apiSecretKey: process.env.SHOPIFY_API_SECRET || "",
  apiVersion: import_server.LATEST_API_VERSION,
  scopes: process.env.SCOPES?.split(","),
  appUrl: process.env.SHOPIFY_APP_URL || "",
  authPathPrefix: "/auth",
  sessionStorage: new import_shopify_app_session_storage_prisma.PrismaSessionStorage(db_server_default),
  distribution: import_server.AppDistribution.AppStore,
  restResources: import__.restResources,
  webhooks: {
    APP_UNINSTALLED: {
      deliveryMethod: import_server.DeliveryMethod.Http,
      callbackUrl: "/webhooks"
    }
  },
  hooks: {
    afterAuth: async ({ session }) => {
      console.log("session================", session), shopify2.registerWebhooks({ session });
    }
  },
  ...process.env.SHOP_CUSTOM_DOMAIN ? { customShopDomains: [process.env.SHOP_CUSTOM_DOMAIN] } : {}
});
var addDocumentResponseHeaders = shopify2.addDocumentResponseHeaders, authenticate = shopify2.authenticate, unauthenticated = shopify2.unauthenticated, login = shopify2.login, registerWebhooks = shopify2.registerWebhooks, sessionStorage = shopify2.sessionStorage;

// app/entry.server.jsx
var import_jsx_dev_runtime = require("react/jsx-dev-runtime"), ABORT_DELAY = 5e3;
async function handleRequest(request, responseStatusCode, responseHeaders, remixContext, _loadContext) {
  addDocumentResponseHeaders(request, responseHeaders);
  let callbackName = (0, import_isbot.default)(request.headers.get("user-agent")) ? "onAllReady" : "onShellReady";
  return new Promise((resolve, reject) => {
    let { pipe, abort } = (0, import_server2.renderToPipeableStream)(
      /* @__PURE__ */ (0, import_jsx_dev_runtime.jsxDEV)(
        import_react.RemixServer,
        {
          context: remixContext,
          url: request.url,
          abortDelay: ABORT_DELAY
        },
        void 0,
        !1,
        {
          fileName: "app/entry.server.jsx",
          lineNumber: 25,
          columnNumber: 7
        },
        this
      ),
      {
        [callbackName]: () => {
          let body = new import_stream.PassThrough();
          responseHeaders.set("Content-Type", "text/html"), resolve(
            new Response(body, {
              headers: responseHeaders,
              status: responseStatusCode
            })
          ), pipe(body);
        },
        onShellError(error) {
          reject(error);
        },
        onError(error) {
          responseStatusCode = 500, console.error(error);
        }
      }
    );
    setTimeout(abort, ABORT_DELAY);
  });
}

// app/root.jsx
var root_exports = {};
__export(root_exports, {
  default: () => App
});
var import_react2 = require("@remix-run/react"), import_jsx_dev_runtime2 = require("react/jsx-dev-runtime");
function App() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("html", { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("head", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("meta", { charSet: "utf-8" }, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 14,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("meta", { name: "viewport", content: "width=device-width,initial-scale=1" }, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 15,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Meta, {}, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 16,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Links, {}, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 17,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/root.jsx",
      lineNumber: 13,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)("body", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Outlet, {}, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 20,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.ScrollRestoration, {}, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 21,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.LiveReload, {}, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 22,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime2.jsxDEV)(import_react2.Scripts, {}, void 0, !1, {
        fileName: "app/root.jsx",
        lineNumber: 23,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/root.jsx",
      lineNumber: 19,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/root.jsx",
    lineNumber: 12,
    columnNumber: 5
  }, this);
}

// app/routes/app.additional.jsx
var app_additional_exports = {};
__export(app_additional_exports, {
  default: () => AdditionalPage,
  links: () => links
});
var import_polaris = require("@shopify/polaris");

// app/routes/assets/app.additional.css
var app_additional_default = "/build/_assets/app.additional-ZKV6WDBF.css";

// app/routes/app.additional.jsx
var import_react3 = require("react"), import_jsx_dev_runtime3 = require("react/jsx-dev-runtime"), links = () => [{ rel: "stylesheet", href: app_additional_default }];
function AdditionalPage() {
  let [data, setData] = (0, import_react3.useState)([]);
  (0, import_react3.useEffect)(() => {
    getListingData();
  }, []);
  let getListingData = async () => {
    console.log("getListingData======================"), fetch("http://localhost:3001/api/order/listings", {
      method: "GET"
    }).then((response) => {
      response.json().then((res) => {
        setData(res.items), console.log(res, "RESSSSSSSSSSSSSSSS");
      });
    }).catch((err) => console.log(err));
  };
  return console.log("Response Listing Data", data), /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_polaris.Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("div", { className: "my-listing-page-container", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("ui-title-bar", { title: "My Listing" }, void 0, !1, {
      fileName: "app/routes/app.additional.jsx",
      lineNumber: 45,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("table", { className: "my-listing-layout", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("thead", { className: "my-listing-layout-thead", children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("tr", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Order ID" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 49,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Shipment Title" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 50,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Pickup" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 51,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Delivery" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 52,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Customer" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 53,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Price" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 55,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("th", { children: "Status" }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 56,
          columnNumber: 15
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 48,
        columnNumber: 13
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 47,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("tbody", { className: "my-listing-layout-tbody", children: data.map((item) => /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)(import_jsx_dev_runtime3.Fragment, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("tr", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: item?.order_id }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 64,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: item?.title }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 65,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: [
          item.pick_up_address,
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { children: [
            "Pickup: ",
            item.pick_up_date
          ] }, void 0, !0, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 68,
            columnNumber: 23
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 66,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: [
          item.delivery_address,
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { children: [
            "Delivery: ",
            item.delivery_date
          ] }, void 0, !0, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 72,
            columnNumber: 23
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 70,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: [
          item.customer_name,
          /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("p", { children: item.mobile_no }, void 0, !1, {
            fileName: "app/routes/app.additional.jsx",
            lineNumber: 76,
            columnNumber: 23
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 74,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: item.price }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 78,
          columnNumber: 21
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime3.jsxDEV)("td", { children: item.status }, void 0, !1, {
          fileName: "app/routes/app.additional.jsx",
          lineNumber: 79,
          columnNumber: 21
        }, this)
      ] }, item?.order_id, !0, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 63,
        columnNumber: 19
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 62,
        columnNumber: 17
      }, this)) }, void 0, !1, {
        fileName: "app/routes/app.additional.jsx",
        lineNumber: 59,
        columnNumber: 11
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/app.additional.jsx",
      lineNumber: 46,
      columnNumber: 9
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/app.additional.jsx",
    lineNumber: 44,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/app.additional.jsx",
    lineNumber: 43,
    columnNumber: 5
  }, this);
}

// app/routes/app.details.jsx
var app_details_exports = {};
__export(app_details_exports, {
  default: () => Details,
  links: () => links2
});
var import_polaris2 = require("@shopify/polaris");

// app/routes/assets/White_Box.jpg
var White_Box_default = "/build/_assets/White_Box-JVYMVTK7.jpg";

// app/routes/assets/details_page.css
var details_page_default = "/build/_assets/details_page-VARWTBO6.css";

// app/routes/app.details.jsx
var import_jsx_dev_runtime4 = require("react/jsx-dev-runtime"), links2 = () => [{ rel: "stylesheet", href: details_page_default }];
function Details() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("ui-title-bar", { title: "Shipment Details" }, void 0, !1, {
      fileName: "app/routes/app.details.jsx",
      lineNumber: 22,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Card, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "first-conatiner", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Card, { roundedAbove: "md", background: "bg-surface-secondary", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { children: "Your shipment is now listed on uShip with an offer price of $150.00 We will notify you when a service provider accepts your offer price, or if there are questions your shipment. You'll have 24 hours or less to when a service providers accepts your price." }, void 0, !1, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 27,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("a", { href: "/path", className: "blue", children: "Go to uShip \u22D9" }, void 0, !1, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 34,
          columnNumber: 15
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 26,
        columnNumber: 13
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 25,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-container", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-container-wrapper", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Card, { roundedAbove: "md", background: "bg-surface-secondary", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Text, { as: "h2", variant: "bodyMd", children: "Shipment Listing Information" }, void 0, !1, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 42,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-inside-container", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-image", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("img", { src: White_Box_default, alt: "Logo" }, void 0, !1, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 47,
              columnNumber: 21
            }, this) }, void 0, !1, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 46,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-table-first", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-table-first-wrapper", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-wrapper-container", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " Shipment ID:" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 53,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "740152872" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 54,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 52,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " Shopify ID:" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 57,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "AH642578SD" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 58,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 56,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " Offer:" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 61,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "$170" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 62,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 60,
                    columnNumber: 25
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 51,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-wrapper-container", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "status-shipment product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "status", children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " Status:" }, void 0, !1, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 68,
                        columnNumber: 29
                      }, this),
                      " ",
                      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "blue", children: "Active" }, void 0, !1, {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 69,
                        columnNumber: 29
                      }, this)
                    ] }, void 0, !0, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 67,
                      columnNumber: 27
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "shipment-track", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("a", { href: "/path", children: "Track Shipment" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 73,
                      columnNumber: 29
                    }, this) }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 71,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 66,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " Shipment Title:" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 77,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "5 items for transaction to torronto" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 78,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 76,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "product-label-data", children: [
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: "Date Listed:" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 81,
                      columnNumber: 27
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "10/23/2023 7:08 AM CST" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 82,
                      columnNumber: 27
                    }, this),
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " End Date:" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 83,
                      columnNumber: 27
                    }, this),
                    " ",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "10/26/2023 11:59 PM CST" }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 84,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 80,
                    columnNumber: 25
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 65,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 50,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-table-second", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: "Customer:" }, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 90,
                    columnNumber: 25
                  }, this),
                  " ",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "Jason Henry" }, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 91,
                    columnNumber: 25
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 89,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "top", children: [
                  "Contact: (316) 555-0116 ",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("br", {}, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 94,
                    columnNumber: 49
                  }, this),
                  "Mail:",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "blue", children: " jason.h-120@gmail.com" }, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 96,
                    columnNumber: 25
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 93,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "top", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: " uShip Listing:" }, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 99,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "blue", children: [
                    "www.uship.com/shipment/7401528...",
                    /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "link-svg", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                      "svg",
                      {
                        xmlns: "http://www.w3.org/2000/svg",
                        width: "16",
                        height: "16",
                        viewBox: "0 0 16 16",
                        fill: "none",
                        children: [
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M12 8.66667V12.6667C12 13.0203 11.8595 13.3594 11.6095 13.6095C11.3594 13.8595 11.0203 14 10.6667 14H3.33333C2.97971 14 2.64057 13.8595 2.39052 13.6095C2.14048 13.3594 2 13.0203 2 12.6667V5.33333C2 4.97971 2.14048 4.64057 2.39052 4.39052C2.64057 4.14048 2.97971 4 3.33333 4H7.33333",
                              stroke: "#0083BB",
                              "stroke-width": "1.33333",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 110,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M10 2H14V6",
                              stroke: "#0083BB",
                              "stroke-width": "1.33333",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 117,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M6.66675 9.33333L14.0001 2",
                              stroke: "#0083BB",
                              "stroke-width": "1.33333",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 124,
                              columnNumber: 31
                            },
                            this
                          )
                        ]
                      },
                      void 0,
                      !0,
                      {
                        fileName: "app/routes/app.details.jsx",
                        lineNumber: 103,
                        columnNumber: 29
                      },
                      this
                    ) }, void 0, !1, {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 102,
                      columnNumber: 27
                    }, this)
                  ] }, void 0, !0, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 100,
                    columnNumber: 25
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 98,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "top", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: "Carrier Contact: ---" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 136,
                  columnNumber: 25
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 135,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 88,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 49,
              columnNumber: 19
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 45,
            columnNumber: 17
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 41,
          columnNumber: 15
        }, this) }, void 0, !1, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 40,
          columnNumber: 13
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "middle-container-aside", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Card, { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Text, { as: "h2", variant: "bodyMd", children: "Origin, Destination, & Route Information" }, void 0, !1, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 147,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "address-tab-wrapper", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "address-tab", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "data-tab", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "info-orgin-wrapper", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "way-svg", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                    "svg",
                    {
                      xmlns: "http://www.w3.org/2000/svg",
                      width: "20",
                      height: "20",
                      viewBox: "0 0 20 20",
                      fill: "none",
                      children: [
                        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("g", { "clip-path": "url(#clip0_218_101)", children: [
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M1.66675 10H4.16675",
                              stroke: "black",
                              "stroke-width": "1.66667",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 163,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M15.8333 10H18.3333",
                              stroke: "black",
                              "stroke-width": "1.66667",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 170,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M10 1.66666V4.16666",
                              stroke: "black",
                              "stroke-width": "1.66667",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 177,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M10 15.8333V18.3333",
                              stroke: "black",
                              "stroke-width": "1.66667",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 184,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M10.0001 15.8333C13.2217 15.8333 15.8334 13.2217 15.8334 10C15.8334 6.77834 13.2217 4.16666 10.0001 4.16666C6.77842 4.16666 4.16675 6.77834 4.16675 10C4.16675 13.2217 6.77842 15.8333 10.0001 15.8333Z",
                              stroke: "black",
                              "stroke-width": "1.66667",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 191,
                              columnNumber: 31
                            },
                            this
                          ),
                          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                            "path",
                            {
                              d: "M10 12.5C11.3807 12.5 12.5 11.3807 12.5 10C12.5 8.61929 11.3807 7.5 10 7.5C8.61929 7.5 7.5 8.61929 7.5 10C7.5 11.3807 8.61929 12.5 10 12.5Z",
                              stroke: "black",
                              "stroke-width": "1.66667",
                              "stroke-linecap": "round",
                              "stroke-linejoin": "round"
                            },
                            void 0,
                            !1,
                            {
                              fileName: "app/routes/app.details.jsx",
                              lineNumber: 198,
                              columnNumber: 31
                            },
                            this
                          )
                        ] }, void 0, !0, {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 162,
                          columnNumber: 29
                        }, this),
                        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("defs", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("clipPath", { id: "clip0_218_101", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("rect", { width: "20", height: "20", fill: "white" }, void 0, !1, {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 208,
                          columnNumber: 33
                        }, this) }, void 0, !1, {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 207,
                          columnNumber: 31
                        }, this) }, void 0, !1, {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 206,
                          columnNumber: 29
                        }, this)
                      ]
                    },
                    void 0,
                    !0,
                    {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 155,
                      columnNumber: 27
                    },
                    this
                  ) }, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 154,
                    columnNumber: 25
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h3", { className: "product-content", children: "ORIGIN:" }, void 0, !1, {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 213,
                    columnNumber: 25
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 153,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("a", { href: "/path", className: "blue", children: "Edit" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 218,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 152,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { children: "600 Garrison CT APT 201 New Castle, Delware 19720 Pickup: On 10/23/2023" }, void 0, !1, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 222,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 151,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "address-tab", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "data-tab-data", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "way-svg", children: [
                  " ",
                  /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                    "svg",
                    {
                      xmlns: "http://www.w3.org/2000/svg",
                      width: "20",
                      height: "20",
                      viewBox: "0 0 20 20",
                      fill: "none",
                      children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                        "path",
                        {
                          d: "M2.5 9.16667L18.3333 1.66667L10.8333 17.5L9.16667 10.8333L2.5 9.16667Z",
                          stroke: "black",
                          "stroke-width": "1.66667",
                          "stroke-linecap": "round",
                          "stroke-linejoin": "round"
                        },
                        void 0,
                        !1,
                        {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 238,
                          columnNumber: 27
                        },
                        this
                      )
                    },
                    void 0,
                    !1,
                    {
                      fileName: "app/routes/app.details.jsx",
                      lineNumber: 231,
                      columnNumber: 25
                    },
                    this
                  )
                ] }, void 0, !0, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 229,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h3", { className: "product-content", children: "ROUTE" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 247,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 228,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { children: "Est. Distance: 122.00 Miles" }, void 0, !1, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 249,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 227,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "address-tab", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "data-tab-data", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { className: "way-svg", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                  "svg",
                  {
                    xmlns: "http://www.w3.org/2000/svg",
                    width: "20",
                    height: "20",
                    viewBox: "0 0 20 20",
                    fill: "none",
                    children: [
                      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                        "path",
                        {
                          d: "M16.6666 8.33333C16.6666 13.3333 9.99992 18.3333 9.99992 18.3333C9.99992 18.3333 3.33325 13.3333 3.33325 8.33333C3.33325 6.56522 4.03563 4.86953 5.28587 3.61929C6.53612 2.36905 8.23181 1.66667 9.99992 1.66667C11.768 1.66667 13.4637 2.36905 14.714 3.61929C15.9642 4.86953 16.6666 6.56522 16.6666 8.33333Z",
                          stroke: "black",
                          "stroke-width": "1.66667",
                          "stroke-linecap": "round",
                          "stroke-linejoin": "round"
                        },
                        void 0,
                        !1,
                        {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 261,
                          columnNumber: 27
                        },
                        this
                      ),
                      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(
                        "path",
                        {
                          d: "M10 10.8333C11.3807 10.8333 12.5 9.71404 12.5 8.33333C12.5 6.95262 11.3807 5.83333 10 5.83333C8.61929 5.83333 7.5 6.95262 7.5 8.33333C7.5 9.71404 8.61929 10.8333 10 10.8333Z",
                          stroke: "black",
                          "stroke-width": "1.66667",
                          "stroke-linecap": "round",
                          "stroke-linejoin": "round"
                        },
                        void 0,
                        !1,
                        {
                          fileName: "app/routes/app.details.jsx",
                          lineNumber: 268,
                          columnNumber: 27
                        },
                        this
                      )
                    ]
                  },
                  void 0,
                  !0,
                  {
                    fileName: "app/routes/app.details.jsx",
                    lineNumber: 254,
                    columnNumber: 25
                  },
                  this
                ) }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 253,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h3", { className: "product-content", children: "DESTINATION" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 277,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 252,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { children: "584 Settlers Lane New York, NY 10007 Delivery: Before 10/30/2023" }, void 0, !1, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 279,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 251,
              columnNumber: 19
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 150,
            columnNumber: 17
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 146,
          columnNumber: 15
        }, this) }, void 0, !1, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 145,
          columnNumber: 13
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 39,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "last-conatiner", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Card, { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)(import_polaris2.Text, { as: "h2", variant: "bodyMd", children: "Household Goods - Furniture" }, void 0, !1, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 290,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "household-wrapper", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "household-product-details border-bottom", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("h2", { className: "product-content", children: "5 ITEMS FOR TRANSPORT " }, void 0, !1, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 296,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Total furniture items: ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "5" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 298,
                  columnNumber: 46
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 297,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 295,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Total Weight: ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "286.6 lbs" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 303,
                  columnNumber: 37
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 302,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Palletized: ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "No" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 306,
                  columnNumber: 35
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 305,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 301,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Crated:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "No" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 311,
                  columnNumber: 30
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 310,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Stackable:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "No" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 314,
                  columnNumber: 33
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 313,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 309,
              columnNumber: 19
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 294,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "household-product-details border-bottom", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
              "Item 1: ",
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "Dining Table" }, void 0, !1, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 321,
                columnNumber: 31
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 320,
              columnNumber: 21
            }, this) }, void 0, !1, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 319,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Quantity:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "1" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 326,
                  columnNumber: 32
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 325,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Dimensions:",
                " ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "L: 3 ft 11 in. W: 2 ft 11 in. H: 2 ft 0 in." }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 330,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 328,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Weight:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "110.2 lbs" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 333,
                  columnNumber: 30
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 332,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 324,
              columnNumber: 19
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 318,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "household-product-details", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
              "Item 1: ",
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "Dining Table" }, void 0, !1, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 340,
                columnNumber: 31
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 339,
              columnNumber: 21
            }, this) }, void 0, !1, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 338,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("div", { className: "about-product-data", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Quantity:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "1" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 345,
                  columnNumber: 32
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 344,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Dimensions:",
                " ",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "L: 3 ft 11 in. W: 2 ft 11 in. H: 2 ft 0 in." }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 349,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 347,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("p", { className: "product-content", children: [
                "Weight:",
                /* @__PURE__ */ (0, import_jsx_dev_runtime4.jsxDEV)("span", { children: "110.2 lbs" }, void 0, !1, {
                  fileName: "app/routes/app.details.jsx",
                  lineNumber: 352,
                  columnNumber: 30
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.details.jsx",
                lineNumber: 351,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.details.jsx",
              lineNumber: 343,
              columnNumber: 19
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.details.jsx",
            lineNumber: 337,
            columnNumber: 17
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.details.jsx",
          lineNumber: 293,
          columnNumber: 15
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 289,
        columnNumber: 13
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.details.jsx",
        lineNumber: 288,
        columnNumber: 11
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/app.details.jsx",
      lineNumber: 24,
      columnNumber: 9
    }, this) }, void 0, !1, {
      fileName: "app/routes/app.details.jsx",
      lineNumber: 23,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/app.details.jsx",
    lineNumber: 21,
    columnNumber: 5
  }, this);
}

// app/routes/app._index.jsx
var app_index_exports = {};
__export(app_index_exports, {
  action: () => action,
  default: () => Index,
  links: () => links3,
  loader: () => loader
});
var import_react4 = require("react");

// app/routes/assets/styles.css
var styles_default = "/build/_assets/styles-2SRR7RMW.css";

// app/routes/app._index.jsx
var import_node2 = require("@remix-run/node"), import_react5 = require("@remix-run/react"), import_polaris3 = require("@shopify/polaris");

// app/routes/assets/logo.jsx
var import_jsx_dev_runtime5 = require("react/jsx-dev-runtime"), Logo = () => /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
  "svg",
  {
    width: "71",
    height: "36",
    viewBox: "0 0 71 36",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg",
    children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M41.3926 19.5992H36.1616V15.0412H31.4906V30.2032H36.1616V24.2672H41.3926V30.2032H46.0636V15.0152H41.3926V19.5992Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 10,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M47.554 30.222H52.222V15.032H47.554V30.222Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 16,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M49.8995 8.8836C48.4095 8.8676 47.1885 10.0626 47.1725 11.5526C47.1725 11.5716 47.1725 11.5906 47.1725 11.6096C47.1715 13.1156 48.3925 14.3376 49.8985 14.3376C51.4045 14.3386 52.6255 13.1186 52.6265 11.6116C52.6275 10.1056 51.4065 8.8846 49.9005 8.8836H49.8995Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 22,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M61.3934 25.8396C59.7504 25.8186 58.4234 24.4916 58.4014 22.8486C58.4014 21.2566 59.8004 19.8606 61.3934 19.8606C62.9864 19.8606 64.3814 21.2606 64.3814 22.8486C64.3814 24.4366 62.9854 25.8396 61.3934 25.8396ZM61.3934 15.4536C60.3954 15.4486 59.4064 15.6536 58.4934 16.0566L58.4014 16.0976V15.0376H53.6294V33.4306H58.4014V29.6246L58.4934 29.6656C59.4064 30.0676 60.3954 30.2726 61.3934 30.2666C65.5394 30.2666 68.9004 27.0056 68.9004 22.8596C68.9004 18.7136 65.5394 15.3526 61.3934 15.4536Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 28,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M11.0857 22.4418C11.0857 24.1568 9.6167 25.6068 7.8797 25.6068C6.1417 25.6068 4.6707 24.1638 4.6707 22.4418V15.0558H-0.000301361V22.4418C-0.000301361 26.7278 3.5347 30.2168 7.8747 30.2168C12.2147 30.2168 15.7487 26.7298 15.7487 22.4418V15.0558H11.0857V22.4418Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 34,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M69.0315 16.9757H68.7405V16.6367H69.0645C69.1235 16.6317 69.1825 16.6487 69.2305 16.6827C69.2655 16.7127 69.2845 16.7567 69.2835 16.8027C69.2835 16.8347 69.2735 16.8667 69.2555 16.8947C69.2375 16.9227 69.2095 16.9437 69.1775 16.9547C69.1305 16.9697 69.0805 16.9767 69.0315 16.9757ZM69.2385 17.1597L69.1715 17.0907L69.2595 17.0527C69.2945 17.0387 69.3255 17.0177 69.3515 16.9897C69.4015 16.9387 69.4275 16.8697 69.4255 16.7987C69.4255 16.7427 69.4105 16.6887 69.3815 16.6407C69.3555 16.5967 69.3145 16.5617 69.2665 16.5427C69.1985 16.5207 69.1265 16.5117 69.0555 16.5147H68.5955V17.5517H68.7365V17.0927H68.8925C68.9175 17.0897 68.9435 17.0897 68.9685 17.0927C68.9905 17.0977 69.0115 17.1077 69.0305 17.1197C69.0565 17.1397 69.0795 17.1627 69.0985 17.1897C69.1235 17.2207 69.1545 17.2657 69.1965 17.3317L69.3345 17.5477H69.5115L69.3295 17.2627C69.3025 17.2257 69.2725 17.1907 69.2385 17.1597Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 40,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M69.0223 17.8787C68.5583 17.8797 68.1813 17.5037 68.1813 17.0397C68.1803 16.5767 68.5563 16.1997 69.0193 16.1987C69.4833 16.1977 69.8603 16.5737 69.8613 17.0377V17.0397C69.8593 17.5027 69.4843 17.8767 69.0223 17.8777V17.8787ZM69.0223 16.0607C68.4823 16.0607 68.0443 16.4987 68.0443 17.0387C68.0443 17.5787 68.4823 18.0167 69.0223 18.0167C69.5623 18.0167 70.0003 17.5787 70.0003 17.0387C69.9993 16.4987 69.5623 16.0617 69.0223 16.0607Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 46,
          columnNumber: 7
        },
        this
      ),
      /* @__PURE__ */ (0, import_jsx_dev_runtime5.jsxDEV)(
        "path",
        {
          "fill-rule": "evenodd",
          "clip-rule": "evenodd",
          d: "M33.29 7.1342L37.34 8.5992L33.31 0.000198364L24.712 4.0332L28.414 5.3712L28.39 5.4342C28.1 6.1712 27.849 6.7252 27.75 6.8442C27.605 7.0162 27.407 7.2512 26.278 7.3262C26.005 7.3442 25.777 7.3522 25.557 7.3522H25.059C24.507 7.3582 23.958 7.4082 23.414 7.5012C21.812 7.7302 20.604 8.2682 19.485 9.2272C18.556 10.0302 17.631 11.0652 17.148 13.2142C16.806 14.7372 17.033 16.2472 17.822 17.7032C18.314 18.6152 19.439 20.0282 21.998 21.5862C23.148 22.2842 23.938 22.8272 24.316 23.3772C24.569 23.7472 24.675 24.1492 24.609 24.4812C24.578 24.6652 24.475 24.8282 24.321 24.9342C24.026 25.1252 23.451 25.2842 22.843 25.4512L22.803 25.4612C21.87 25.7242 20.713 26.0362 19.784 26.6432C18.367 27.5612 17.947 28.2772 16.712 33.7522L16.638 34.0762L21.279 35.3992L21.367 35.0442C21.805 33.2782 22.154 32.2402 22.397 31.9582C22.674 31.6382 22.833 31.5062 23.534 31.2882L23.968 31.1572L24.148 31.1032C25.185 30.8192 26.192 30.4382 27.159 29.9672C28.917 29.1102 30.161 26.9942 30.405 24.4442C30.668 21.7402 29.688 20.2682 28.596 19.0822C27.844 18.2682 24.564 16.1552 24.095 15.8762C23.518 15.5332 22.842 14.9572 22.895 14.1222C22.938 13.4662 23.644 13.1782 23.83 13.1342C23.972 13.1032 24.215 13.0792 24.598 13.0422H24.636C25.169 12.9922 25.973 12.9112 27.118 12.7472C29.311 12.4232 30.491 11.3632 30.896 10.9222C31.906 9.8332 32.711 8.5722 33.274 7.1982L33.29 7.1342Z",
          fill: "#02AAF3"
        },
        void 0,
        !1,
        {
          fileName: "app/routes/assets/logo.jsx",
          lineNumber: 52,
          columnNumber: 7
        },
        this
      )
    ]
  },
  void 0,
  !0,
  {
    fileName: "app/routes/assets/logo.jsx",
    lineNumber: 3,
    columnNumber: 5
  },
  this
);

// app/routes/app._index.jsx
var import_jsx_dev_runtime6 = require("react/jsx-dev-runtime"), links3 = () => [{ rel: "stylesheet", href: styles_default }], loader = async ({ request }) => (await authenticate.admin(request), null);
async function action({ request }) {
  let { admin } = await authenticate.admin(request), color = ["Red", "Orange", "Yellow", "Green"][Math.floor(Math.random() * 4)], responseJson = await (await admin.graphql(
    `#graphql
      mutation populateProduct($input: ProductInput!) {
        productCreate(input: $input) {
          product {
            id
            title
            handle
            status
            variants(first: 10) {
              edges {
                node {
                  id
                  price
                  barcode
                  createdAt
                }
              }
            }
          }
        }
      }`,
    {
      variables: {
        input: {
          title: `${color} Snowboard`,
          variants: [{ price: Math.random() * 100 }]
        }
      }
    }
  )).json();
  return (0, import_node2.json)({
    product: responseJson.data.productCreate.product
  });
}
function Index() {
  let [formState, setformState] = (0, import_react4.useState)({
    email: "",
    password: "",
    unique_key: "",
    access_key: ""
  }), productId = (0, import_react5.useActionData)()?.product?.id.replace(
    "gid://shopify/Product/",
    ""
  ), handleSubmit = (0, import_react4.useCallback)(() => {
  }, []), Openpopup = (0, import_react4.useCallback)(() => {
    let get = document.querySelector("#popup");
    get && get.classList.toggle("hide");
  }, []);
  (0, import_react4.useEffect)(() => {
    productId && shopify.toast.show("Product created");
  }, [productId]);
  let generateProduct = async () => {
    console.log(formState.email), console.log(formState.password);
    let headers2 = new Headers();
    headers2.append("Content-Type", "application/json"), headers2.append("Accept", "application/json"), await fetch("https://api-web.ushipdev.com/oauth/token_authenticated", {
      credentials: "include",
      // headers: {
      //   "Access-Control-Allow-Origin": "*", // Replace with your allowed origin(s)
      //   "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
      //   "Access-Control-Allow-Headers":
      //     "Origin, X-Requested-With, Content-Type, Accept",
      //   // 'x-frame-options' : 'none'
      // },
      method: "POST",
      body: JSON.stringify({
        client_id: "djc82cpkrkcbqr3xjtdgkb4u",
        client_secret: "G5tPdQEvty",
        grant_type: "password",
        username: formState.email,
        password: formState.password
      })
    }).then((response) => console.log(response, "RESSS")).catch((err) => console.error(err)), setformState({
      email: "",
      password: "",
      access_key: "",
      unique_key: ""
    });
  }, handleChange = (field, value) => {
    setformState({ ...formState, [field]: value });
  };
  return /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_jsx_dev_runtime6.Fragment, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.VerticalStack, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "login-page-container", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Layout, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "login-page-layout-container", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Card, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "login-page-layout-logo", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(Logo, {}, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 197,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 196,
        columnNumber: 19
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "login-page-layout-text", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Text, { as: "h2", variant: "headingMd", children: "Access Uship Listing" }, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 201,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 200,
        columnNumber: 19
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Form, { onSubmit: handleSubmit, children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.FormLayout, { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(
          import_polaris3.TextField,
          {
            label: "Access Key",
            type: "text",
            autoComplete: "none",
            placeholder: "Enter your access key here",
            name: "username",
            value: formState.access_key,
            onChange: (value) => handleChange("access_key", value),
            requiredIndicator: !0
          },
          void 0,
          !1,
          {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 208,
            columnNumber: 23
          },
          this
        ),
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Submit-Button", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Button, { primary: !0, fullWidth: !0, onClick: generateProduct, children: "Submit" }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 220,
          columnNumber: 25
        }, this) }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 219,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Forget-Button", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Button, { plain: !0, fullWidth: !0, onClick: Openpopup, children: "How to get access key?" }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 225,
          columnNumber: 25
        }, this) }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 224,
          columnNumber: 23
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 207,
        columnNumber: 21
      }, this) }, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 206,
        columnNumber: 19
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/app._index.jsx",
      lineNumber: 195,
      columnNumber: 17
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Access-key-popup hide", id: "popup", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Card, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Text, { as: "h2", variant: "headingMd", children: "Where to find Access key?" }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 241,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("ul", { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("li", { children: [
            "Login to",
            " ",
            /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(
              "a",
              {
                href: "https://login.ushipsandbox.com",
                className: "Access-key-popup-A",
                target: "_blank",
                rel: "noreferrer",
                children: "https://login.ushipsandbox.com/"
              },
              void 0,
              !1,
              {
                fileName: "app/routes/app._index.jsx",
                lineNumber: 247,
                columnNumber: 27
              },
              this
            )
          ] }, void 0, !0, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 245,
            columnNumber: 25
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("li", { children: "Go to (Menu-Name)(Sub Menu-Name)" }, void 0, !1, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 256,
            columnNumber: 25
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("li", { children: "Click on API KEY GENERATION" }, void 0, !1, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 257,
            columnNumber: 25
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 244,
          columnNumber: 23
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("p", { children: "After following above steps you will be able to see below screen and can generate your unique key from here. This key can be used to access your uship account in shopify." }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 259,
          columnNumber: 23
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 240,
        columnNumber: 21
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Access-key-popup-form", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Api-form", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Text, { as: "h2", variant: "headingMd", children: "API KEY GENERATION" }, void 0, !1, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 269,
          columnNumber: 27
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("p", { children: [
          "Generate a unique key below. Once generated, you can copuy/paste your key into your application. Note that generating a new key will invalidate a current existingkey . ",
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("b", { children: "Learn More" }, void 0, !1, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 276,
            columnNumber: 43
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 272,
          columnNumber: 27
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("form", { children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Generate-key-Button", children: /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Button, { onClick: handleSubmit, children: "Generate Key" }, void 0, !1, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 280,
            columnNumber: 31
          }, this) }, void 0, !1, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 279,
            columnNumber: 29
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)("div", { className: "Api-form-input-box", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(
              import_polaris3.TextField,
              {
                label: "YOUR UNIQUE KEY",
                type: "text",
                autoComplete: "none",
                placeholder: "Enter your unique key here",
                name: "username",
                value: formState.unique_key,
                onChange: (value) => handleChange("unique_key", value),
                requiredIndicator: !0
              },
              void 0,
              !1,
              {
                fileName: "app/routes/app._index.jsx",
                lineNumber: 285,
                columnNumber: 31
              },
              this
            ),
            /* @__PURE__ */ (0, import_jsx_dev_runtime6.jsxDEV)(import_polaris3.Button, { onClick: handleSubmit, children: "\u2714" }, void 0, !1, {
              fileName: "app/routes/app._index.jsx",
              lineNumber: 297,
              columnNumber: 31
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app._index.jsx",
            lineNumber: 284,
            columnNumber: 29
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app._index.jsx",
          lineNumber: 278,
          columnNumber: 27
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 268,
        columnNumber: 25
      }, this) }, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 267,
        columnNumber: 23
      }, this) }, void 0, !1, {
        fileName: "app/routes/app._index.jsx",
        lineNumber: 266,
        columnNumber: 21
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/app._index.jsx",
      lineNumber: 239,
      columnNumber: 19
    }, this) }, void 0, !1, {
      fileName: "app/routes/app._index.jsx",
      lineNumber: 238,
      columnNumber: 17
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 194,
    columnNumber: 15
  }, this) }, void 0, !1, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 193,
    columnNumber: 13
  }, this) }, void 0, !1, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 192,
    columnNumber: 11
  }, this) }, void 0, !1, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 191,
    columnNumber: 9
  }, this) }, void 0, !1, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 190,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/app._index.jsx",
    lineNumber: 189,
    columnNumber: 5
  }, this);
}

// app/routes/auth.login/route.jsx
var route_exports = {};
__export(route_exports, {
  action: () => action2,
  default: () => Auth,
  links: () => links4,
  loader: () => loader2
});
var import_react6 = require("react"), import_node3 = require("@remix-run/node"), import_polaris4 = require("@shopify/polaris"), import_react7 = require("@remix-run/react");

// node_modules/@shopify/polaris/build/esm/styles.css
var styles_default2 = "/build/_assets/styles-S2UC6H46.css";

// app/routes/auth.login/error.server.jsx
var import_server3 = require("@shopify/shopify-app-remix/server");
function loginErrorMessage(loginErrors) {
  return loginErrors?.shop === import_server3.LoginErrorType.MissingShop ? { shop: "Please enter your shop domain to log in" } : loginErrors?.shop === import_server3.LoginErrorType.InvalidShop ? { shop: "Please enter a valid shop domain to log in" } : {};
}

// app/routes/auth.login/route.jsx
var import_jsx_dev_runtime7 = require("react/jsx-dev-runtime"), links4 = () => [{ rel: "stylesheet", href: styles_default2 }];
async function loader2({ request }) {
  let errors = loginErrorMessage(await login(request));
  return (0, import_node3.json)({
    errors,
    polarisTranslations: require("@shopify/polaris/locales/en.json")
  });
}
async function action2({ request }) {
  let errors = loginErrorMessage(await login(request));
  return (0, import_node3.json)({
    errors
  });
}
function Auth() {
  let { polarisTranslations } = (0, import_react7.useLoaderData)(), loaderData = (0, import_react7.useLoaderData)(), actionData = (0, import_react7.useActionData)(), [shop, setShop] = (0, import_react6.useState)(""), { errors } = actionData || loaderData;
  return /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_polaris4.AppProvider, { i18n: polarisTranslations, children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_polaris4.Page, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_polaris4.Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_react7.Form, { method: "post", children: /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_polaris4.FormLayout, { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_polaris4.Text, { variant: "headingMd", as: "h2", children: "Log in" }, void 0, !1, {
      fileName: "app/routes/auth.login/route.jsx",
      lineNumber: 51,
      columnNumber: 15
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(
      import_polaris4.TextField,
      {
        type: "text",
        name: "shop",
        label: "Shop domain",
        helpText: "example.myshopify.com",
        value: shop,
        onChange: setShop,
        autoComplete: "on",
        error: errors.shop
      },
      void 0,
      !1,
      {
        fileName: "app/routes/auth.login/route.jsx",
        lineNumber: 54,
        columnNumber: 15
      },
      this
    ),
    /* @__PURE__ */ (0, import_jsx_dev_runtime7.jsxDEV)(import_polaris4.Button, { submit: !0, children: "Log in" }, void 0, !1, {
      fileName: "app/routes/auth.login/route.jsx",
      lineNumber: 64,
      columnNumber: 15
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/auth.login/route.jsx",
    lineNumber: 50,
    columnNumber: 13
  }, this) }, void 0, !1, {
    fileName: "app/routes/auth.login/route.jsx",
    lineNumber: 49,
    columnNumber: 11
  }, this) }, void 0, !1, {
    fileName: "app/routes/auth.login/route.jsx",
    lineNumber: 48,
    columnNumber: 9
  }, this) }, void 0, !1, {
    fileName: "app/routes/auth.login/route.jsx",
    lineNumber: 47,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/auth.login/route.jsx",
    lineNumber: 46,
    columnNumber: 5
  }, this);
}

// app/routes/app.form.jsx
var app_form_exports = {};
__export(app_form_exports, {
  default: () => AdditionalPage2
});
var import_polaris5 = require("@shopify/polaris"), import_jsx_dev_runtime8 = require("react/jsx-dev-runtime");
function AdditionalPage2() {
  return /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Page, { children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("ui-title-bar", { title: "NEW DELIVERY" }, void 0, !1, {
      fileName: "app/routes/app.form.jsx",
      lineNumber: 15,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Layout, { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Layout.Section, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.VerticalStack, { gap: "3", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "header-wrapper", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "Left-menu", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("h3", { children: "CREATE LISTING" }, void 0, !1, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 22,
            columnNumber: 19
          }, this) }, void 0, !1, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 21,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "right-menu", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("h3", { children: "IMPORT FROM" }, void 0, !1, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 25,
              columnNumber: 19
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("svg", { xmlns: "http://www.w3.org/2000/svg", "aria-label": "eBay", role: "img", viewBox: "0 0 512 512", width: "64px", height: "64px", fill: "#000000", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("g", { id: "SVGRepo_bgCarrier", "stroke-width": "0" }, void 0, !1, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 27,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("g", { id: "SVGRepo_tracerCarrier", "stroke-linecap": "round", "stroke-linejoin": "round" }, void 0, !1, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 28,
                columnNumber: 21
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("g", { id: "SVGRepo_iconCarrier", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("rect", { width: "512", height: "512", rx: "15%", fill: "#ffffff" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 30,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("path", { fill: "#e53238", d: "M98 208c-30 0-55 12-55 51 0 31 17 49 56 49 46 0 49-30 49-30h-23s-4 17-28 17c-19 0-32-13-32-31h85v-12c0-17-11-44-52-44zm-1 14c18 0 30 11 30 28H65c0-18 16-28 32-28z" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 31,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("path", { fill: "#0064d2", d: "M150 171v119l-1 16h22l1-14s10 17 39 17c30 0 50-21 50-51 0-28-18-50-50-50-31 0-39 16-39 16v-53zm55 52c21 0 33 15 33 35 0 22-15 36-33 36-22 0-33-17-33-35s10-36 33-36z" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 32,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("path", { fill: "#f5af03", d: "M314 208c-46 0-48 25-48 28h22s2-14 24-14c31 0 27 24 27 24h-27c-35 0-53 11-53 31s17 32 40 32c32 0 41-17 41-17l1 14h20v-62c0-30-25-36-47-36zm25 52s4 35-35 35c-16 0-23-8-23-18 0-17 24-17 58-17z" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 33,
                  columnNumber: 23
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("path", { fill: "#86b817", d: "M348 212h25l37 72 36-72h23l-65 129h-24l18-36z" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 34,
                  columnNumber: 23
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 29,
                columnNumber: 21
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 26,
              columnNumber: 19
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 24,
            columnNumber: 17
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 20,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-wrapper", children: [
          /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("form", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "main-form-wrapper", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "first-type", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "furniture", children: "FURNITURE TYPE" }, void 0, !1, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 43,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("input", { type: "text", placeholder: "What type of furniture? i.e. couch, chair", id: "furniture", name: "furniture" }, void 0, !1, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 44,
                columnNumber: 23
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 42,
              columnNumber: 21
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-section", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-1", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "length", children: "LENGTH" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 48,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "inputDiv-length", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("input", { type: "text", id: "length", name: "length" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 50,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 49,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 47,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-2", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "length", children: "WIDTH" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 54,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "inputDiv-width", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("input", { type: "text", id: "width", name: "width" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 56,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 55,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 53,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-3", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "length", children: "HEIGHT" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 60,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "inputDiv-height", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("input", { type: "text", id: "height", name: "height" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 62,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 61,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 59,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-4", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "forDropDown" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 66,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "dropDown-wrapper", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("select", { name: "mesure", id: "mesure", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "in", children: "in" }, void 0, !1, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 69,
                    columnNumber: 29
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "cm", selected: !0, children: "cm" }, void 0, !1, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 70,
                    columnNumber: 29
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 68,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 67,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 65,
                columnNumber: 23
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 46,
              columnNumber: 21
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "form-section b", children: [
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-5", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "length", children: "WEIGHT" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 77,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "inputDiv-height", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("input", { type: "text", id: "weight", name: "weight" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 79,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 78,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 76,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-6", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "forDropDown" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 83,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "dropDown-wrapper", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("select", { name: "weight", id: "weight", children: [
                  /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "lbs", selected: !0, children: "lbs" }, void 0, !1, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 86,
                    columnNumber: 29
                  }, this),
                  /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("option", { value: "lm", children: "km" }, void 0, !1, {
                    fileName: "app/routes/app.form.jsx",
                    lineNumber: 87,
                    columnNumber: 29
                  }, this)
                ] }, void 0, !0, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 85,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 84,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 82,
                columnNumber: 23
              }, this),
              /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "flex-div-6", children: [
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("label", { htmlFor: "length", children: "QUANTITY" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 92,
                  columnNumber: 25
                }, this),
                /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "inputDiv-height", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("input", { type: "text", id: "qty", name: "qty" }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 94,
                  columnNumber: 27
                }, this) }, void 0, !1, {
                  fileName: "app/routes/app.form.jsx",
                  lineNumber: 93,
                  columnNumber: 25
                }, this)
              ] }, void 0, !0, {
                fileName: "app/routes/app.form.jsx",
                lineNumber: 91,
                columnNumber: 23
              }, this)
            ] }, void 0, !0, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 75,
              columnNumber: 21
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 41,
            columnNumber: 19
          }, this) }, void 0, !1, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 40,
            columnNumber: 17
          }, this),
          /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("aside", { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "side-content", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("div", { className: "side-main-content", children: [
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "content-title", children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("b", { children: "Include Packaging" }, void 0, !1, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 103,
              columnNumber: 52
            }, this) }, void 0, !1, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 103,
              columnNumber: 23
            }, this),
            /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)("p", { className: "content-area", children: "Try to include packaging materials when measuring dimensions. (best estimate)" }, void 0, !1, {
              fileName: "app/routes/app.form.jsx",
              lineNumber: 104,
              columnNumber: 23
            }, this)
          ] }, void 0, !0, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 102,
            columnNumber: 21
          }, this) }, void 0, !1, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 101,
            columnNumber: 19
          }, this) }, void 0, !1, {
            fileName: "app/routes/app.form.jsx",
            lineNumber: 100,
            columnNumber: 17
          }, this)
        ] }, void 0, !0, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 39,
          columnNumber: 15
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 19,
        columnNumber: 13
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 18,
        columnNumber: 11
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 17,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Layout.Section, { secondary: !0, children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Card, { children: /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.VerticalStack, { gap: "2", children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Text, { as: "h2", variant: "headingMd", children: "Details Matter" }, void 0, !1, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 115,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime8.jsxDEV)(import_polaris5.Text, { as: "p", children: "The quotes you get are only as accurate as your listing. Make it as detailed as possible to avoid delays, price increases, and cancellations." }, void 0, !1, {
          fileName: "app/routes/app.form.jsx",
          lineNumber: 118,
          columnNumber: 15
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 114,
        columnNumber: 13
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 113,
        columnNumber: 11
      }, this) }, void 0, !1, {
        fileName: "app/routes/app.form.jsx",
        lineNumber: 112,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/app.form.jsx",
      lineNumber: 16,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/app.form.jsx",
    lineNumber: 14,
    columnNumber: 5
  }, this);
}

// app/routes/webhooks.jsx
var webhooks_exports = {};
__export(webhooks_exports, {
  action: () => action3
});
var action3 = async ({ request }) => {
  let { topic, shop, session, admin } = await authenticate.webhook(request);
  if (!admin)
    throw new Response();
  switch (topic) {
    case "APP_UNINSTALLED":
      session && await db_server_default.session.deleteMany({ where: { shop } });
      break;
    case "CUSTOMERS_DATA_REQUEST":
    case "CUSTOMERS_REDACT":
    case "SHOP_REDACT":
    default:
      throw new Response("Unhandled webhook topic", { status: 404 });
  }
  throw new Response();
};

// app/routes/_index/route.jsx
var route_exports2 = {};
__export(route_exports2, {
  default: () => App2,
  links: () => links5,
  loader: () => loader3
});
var import_node4 = require("@remix-run/node"), import_react8 = require("@remix-run/react");

// app/routes/_index/style.css
var style_default = "/build/_assets/style-M2E3MJNO.css";

// app/routes/_index/route.jsx
var import_jsx_dev_runtime9 = require("react/jsx-dev-runtime"), links5 = () => [{ rel: "stylesheet", href: style_default }];
async function loader3({ request }) {
  let url = new URL(request.url);
  if (url.searchParams.get("shop"))
    throw (0, import_node4.redirect)(`/app?${url.searchParams.toString()}`);
  return (0, import_node4.json)({ showForm: Boolean(login) });
}
function App2() {
  let { showForm } = (0, import_react8.useLoaderData)();
  return /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("div", { className: "index", children: /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("div", { className: "content", children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("h1", { children: "A short heading about [your app]" }, void 0, !1, {
      fileName: "app/routes/_index/route.jsx",
      lineNumber: 26,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("p", { children: "A tagline about [your app] that describes your value proposition." }, void 0, !1, {
      fileName: "app/routes/_index/route.jsx",
      lineNumber: 27,
      columnNumber: 9
    }, this),
    showForm && /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)(import_react8.Form, { method: "post", action: "/auth/login", children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("label", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("span", { children: "Shop domain" }, void 0, !1, {
          fileName: "app/routes/_index/route.jsx",
          lineNumber: 31,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("input", { type: "text", name: "shop" }, void 0, !1, {
          fileName: "app/routes/_index/route.jsx",
          lineNumber: 32,
          columnNumber: 15
        }, this),
        /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("span", { children: "e.g: my-shop-domain.myshopify.com" }, void 0, !1, {
          fileName: "app/routes/_index/route.jsx",
          lineNumber: 33,
          columnNumber: 15
        }, this)
      ] }, void 0, !0, {
        fileName: "app/routes/_index/route.jsx",
        lineNumber: 30,
        columnNumber: 13
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("button", { type: "submit", children: "Log in" }, void 0, !1, {
        fileName: "app/routes/_index/route.jsx",
        lineNumber: 35,
        columnNumber: 13
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/_index/route.jsx",
      lineNumber: 29,
      columnNumber: 9
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("ul", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("li", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("strong", { children: "Product feature" }, void 0, !1, {
          fileName: "app/routes/_index/route.jsx",
          lineNumber: 40,
          columnNumber: 13
        }, this),
        ". Some detail about your feature and its benefit to your customer."
      ] }, void 0, !0, {
        fileName: "app/routes/_index/route.jsx",
        lineNumber: 39,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("li", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("strong", { children: "Product feature" }, void 0, !1, {
          fileName: "app/routes/_index/route.jsx",
          lineNumber: 44,
          columnNumber: 13
        }, this),
        ". Some detail about your feature and its benefit to your customer."
      ] }, void 0, !0, {
        fileName: "app/routes/_index/route.jsx",
        lineNumber: 43,
        columnNumber: 11
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("li", { children: [
        /* @__PURE__ */ (0, import_jsx_dev_runtime9.jsxDEV)("strong", { children: "Product feature" }, void 0, !1, {
          fileName: "app/routes/_index/route.jsx",
          lineNumber: 48,
          columnNumber: 13
        }, this),
        ". Some detail about your feature and its benefit to your customer."
      ] }, void 0, !0, {
        fileName: "app/routes/_index/route.jsx",
        lineNumber: 47,
        columnNumber: 11
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/_index/route.jsx",
      lineNumber: 38,
      columnNumber: 9
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/_index/route.jsx",
    lineNumber: 25,
    columnNumber: 7
  }, this) }, void 0, !1, {
    fileName: "app/routes/_index/route.jsx",
    lineNumber: 24,
    columnNumber: 5
  }, this);
}

// app/routes/auth.$.jsx
var auth_exports = {};
__export(auth_exports, {
  loader: () => loader4
});
async function loader4({ request }) {
  return await authenticate.admin(request), null;
}

// app/routes/config.js
var config_exports = {};
__export(config_exports, {
  getToken: () => getToken
});
var axios = require("axios"), getToken = async () => {
  let token = await axios.post("https://api.ushipsandbox.com/partner/oauth/token_authenticated", {
    client_id: "djc82cpkrkcbqr3xjtdgkb4u",
    client_secret: "G5tPdQEvty",
    grant_type: "password",
    username: "darshan@simformsolutions.com",
    password: "Simform@987"
  });
  return console.log("token auth......... , token", token), token.data;
};

// app/routes/test.jsx
var test_exports = {};
__export(test_exports, {
  default: () => test_default
});
var import_prop_types = __toESM(require("prop-types")), import_react9 = require("react"), import_jsx_dev_runtime10 = require("react/jsx-dev-runtime"), Button3 = ({ text }) => /* @__PURE__ */ (0, import_jsx_dev_runtime10.jsxDEV)("button", { children: text }, void 0, !1, {
  fileName: "app/routes/test.jsx",
  lineNumber: 5,
  columnNumber: 10
}, this);
Button3.propTypes = {
  text: import_prop_types.default.string.isRequired
};
var test_default = Button3;

// app/routes/app.jsx
var app_exports = {};
__export(app_exports, {
  ErrorBoundary: () => ErrorBoundary,
  default: () => App3,
  headers: () => headers,
  links: () => links6,
  loader: () => loader5
});
var import_node5 = require("@remix-run/node"), import_react10 = require("@remix-run/react");
var import_server4 = require("@shopify/shopify-app-remix/server"), import_react11 = require("@shopify/shopify-app-remix/react");
var import_jsx_dev_runtime11 = require("react/jsx-dev-runtime"), links6 = () => [{ rel: "stylesheet", href: styles_default2 }];
async function loader5({ request }) {
  return await authenticate.admin(request), (0, import_node5.json)({ apiKey: process.env.SHOPIFY_API_KEY });
}
function App3() {
  let { apiKey } = (0, import_react10.useLoaderData)();
  return /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)(import_react11.AppProvider, { isEmbeddedApp: !0, apiKey, children: [
    /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)("ui-nav-menu", { children: [
      /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)(import_react10.Link, { to: "/app", rel: "home", children: "Home" }, void 0, !1, {
        fileName: "app/routes/app.jsx",
        lineNumber: 23,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)(import_react10.Link, { to: "/app/additional", children: "Listing page" }, void 0, !1, {
        fileName: "app/routes/app.jsx",
        lineNumber: 26,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)(import_react10.Link, { to: "/app/details", children: "Details page" }, void 0, !1, {
        fileName: "app/routes/app.jsx",
        lineNumber: 27,
        columnNumber: 9
      }, this),
      /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)(import_react10.Link, { to: "/app/form", children: "Form page" }, void 0, !1, {
        fileName: "app/routes/app.jsx",
        lineNumber: 28,
        columnNumber: 9
      }, this)
    ] }, void 0, !0, {
      fileName: "app/routes/app.jsx",
      lineNumber: 22,
      columnNumber: 7
    }, this),
    /* @__PURE__ */ (0, import_jsx_dev_runtime11.jsxDEV)(import_react10.Outlet, {}, void 0, !1, {
      fileName: "app/routes/app.jsx",
      lineNumber: 30,
      columnNumber: 7
    }, this)
  ] }, void 0, !0, {
    fileName: "app/routes/app.jsx",
    lineNumber: 21,
    columnNumber: 5
  }, this);
}
function ErrorBoundary() {
  return import_server4.boundary.error((0, import_react10.useRouteError)());
}
var headers = (headersArgs) => import_server4.boundary.headers(headersArgs);

// server-assets-manifest:@remix-run/dev/assets-manifest
var assets_manifest_default = { entry: { module: "/build/entry.client-WJCVYG7P.js", imports: ["/build/_shared/chunk-ZWGWGGVF.js", "/build/_shared/chunk-XNST2UG7.js", "/build/_shared/chunk-GIAAE3CH.js", "/build/_shared/chunk-XU7DNSPJ.js", "/build/_shared/chunk-BOXFZXVX.js", "/build/_shared/chunk-BYNBTF7W.js", "/build/_shared/chunk-UWV35TSL.js", "/build/_shared/chunk-PNG5AS42.js"] }, routes: { root: { id: "root", parentId: void 0, path: "", index: void 0, caseSensitive: void 0, module: "/build/root-MMW6TQ55.js", imports: void 0, hasAction: !1, hasLoader: !1, hasErrorBoundary: !1 }, "routes/_index": { id: "routes/_index", parentId: "root", path: void 0, index: !0, caseSensitive: void 0, module: "/build/routes/_index-UTNVZT3M.js", imports: ["/build/_shared/chunk-3GJP5LZF.js", "/build/_shared/chunk-G7CHZRZX.js"], hasAction: !1, hasLoader: !0, hasErrorBoundary: !1 }, "routes/app": { id: "routes/app", parentId: "root", path: "app", index: void 0, caseSensitive: void 0, module: "/build/routes/app-UTFI7J6B.js", imports: ["/build/_shared/chunk-NMZL6IDN.js", "/build/_shared/chunk-SU66BP3D.js", "/build/_shared/chunk-7SKDU3VC.js", "/build/_shared/chunk-G7CHZRZX.js", "/build/_shared/chunk-RYZ2NSIR.js", "/build/_shared/chunk-HEETWZQQ.js"], hasAction: !1, hasLoader: !0, hasErrorBoundary: !0 }, "routes/app._index": { id: "routes/app._index", parentId: "routes/app", path: void 0, index: !0, caseSensitive: void 0, module: "/build/routes/app._index-74KZTSGT.js", imports: void 0, hasAction: !0, hasLoader: !0, hasErrorBoundary: !1 }, "routes/app.additional": { id: "routes/app.additional", parentId: "routes/app", path: "additional", index: void 0, caseSensitive: void 0, module: "/build/routes/app.additional-MYXOXMTA.js", imports: void 0, hasAction: !1, hasLoader: !1, hasErrorBoundary: !1 }, "routes/app.details": { id: "routes/app.details", parentId: "routes/app", path: "details", index: void 0, caseSensitive: void 0, module: "/build/routes/app.details-NP42NMJQ.js", imports: void 0, hasAction: !1, hasLoader: !1, hasErrorBoundary: !1 }, "routes/app.form": { id: "routes/app.form", parentId: "routes/app", path: "form", index: void 0, caseSensitive: void 0, module: "/build/routes/app.form-2C774YXV.js", imports: void 0, hasAction: !1, hasLoader: !1, hasErrorBoundary: !1 }, "routes/auth.$": { id: "routes/auth.$", parentId: "root", path: "auth/*", index: void 0, caseSensitive: void 0, module: "/build/routes/auth.$-4B5WQABX.js", imports: void 0, hasAction: !1, hasLoader: !0, hasErrorBoundary: !1 }, "routes/auth.login": { id: "routes/auth.login", parentId: "root", path: "auth/login", index: void 0, caseSensitive: void 0, module: "/build/routes/auth.login-GXCH72NO.js", imports: ["/build/_shared/chunk-3GJP5LZF.js", "/build/_shared/chunk-7SKDU3VC.js", "/build/_shared/chunk-G7CHZRZX.js", "/build/_shared/chunk-RYZ2NSIR.js", "/build/_shared/chunk-HEETWZQQ.js"], hasAction: !0, hasLoader: !0, hasErrorBoundary: !1 }, "routes/config": { id: "routes/config", parentId: "root", path: "config", index: void 0, caseSensitive: void 0, module: "/build/routes/config-3X7SI4H4.js", imports: void 0, hasAction: !1, hasLoader: !1, hasErrorBoundary: !1 }, "routes/test": { id: "routes/test", parentId: "root", path: "test", index: void 0, caseSensitive: void 0, module: "/build/routes/test-COJP6KYY.js", imports: ["/build/_shared/chunk-HEETWZQQ.js"], hasAction: !1, hasLoader: !1, hasErrorBoundary: !1 }, "routes/webhooks": { id: "routes/webhooks", parentId: "root", path: "webhooks", index: void 0, caseSensitive: void 0, module: "/build/routes/webhooks-JFV2P4HI.js", imports: void 0, hasAction: !0, hasLoader: !1, hasErrorBoundary: !1 } }, version: "bc57e1d6", hmr: { runtime: "/build/_shared/chunk-BYNBTF7W.js", timestamp: 1700128019566 }, url: "/build/manifest-BC57E1D6.js" };

// server-entry-module:@remix-run/dev/server-build
var mode = "development", assetsBuildDirectory = "public/build", future = {}, publicPath = "/build/", entry = { module: entry_server_exports }, routes = {
  root: {
    id: "root",
    parentId: void 0,
    path: "",
    index: void 0,
    caseSensitive: void 0,
    module: root_exports
  },
  "routes/app.additional": {
    id: "routes/app.additional",
    parentId: "routes/app",
    path: "additional",
    index: void 0,
    caseSensitive: void 0,
    module: app_additional_exports
  },
  "routes/app.details": {
    id: "routes/app.details",
    parentId: "routes/app",
    path: "details",
    index: void 0,
    caseSensitive: void 0,
    module: app_details_exports
  },
  "routes/app._index": {
    id: "routes/app._index",
    parentId: "routes/app",
    path: void 0,
    index: !0,
    caseSensitive: void 0,
    module: app_index_exports
  },
  "routes/auth.login": {
    id: "routes/auth.login",
    parentId: "root",
    path: "auth/login",
    index: void 0,
    caseSensitive: void 0,
    module: route_exports
  },
  "routes/app.form": {
    id: "routes/app.form",
    parentId: "routes/app",
    path: "form",
    index: void 0,
    caseSensitive: void 0,
    module: app_form_exports
  },
  "routes/webhooks": {
    id: "routes/webhooks",
    parentId: "root",
    path: "webhooks",
    index: void 0,
    caseSensitive: void 0,
    module: webhooks_exports
  },
  "routes/_index": {
    id: "routes/_index",
    parentId: "root",
    path: void 0,
    index: !0,
    caseSensitive: void 0,
    module: route_exports2
  },
  "routes/auth.$": {
    id: "routes/auth.$",
    parentId: "root",
    path: "auth/*",
    index: void 0,
    caseSensitive: void 0,
    module: auth_exports
  },
  "routes/config": {
    id: "routes/config",
    parentId: "root",
    path: "config",
    index: void 0,
    caseSensitive: void 0,
    module: config_exports
  },
  "routes/test": {
    id: "routes/test",
    parentId: "root",
    path: "test",
    index: void 0,
    caseSensitive: void 0,
    module: test_exports
  },
  "routes/app": {
    id: "routes/app",
    parentId: "root",
    path: "app",
    index: void 0,
    caseSensitive: void 0,
    module: app_exports
  }
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  assets,
  assetsBuildDirectory,
  entry,
  future,
  mode,
  publicPath,
  routes
});
//# sourceMappingURL=index.js.map
